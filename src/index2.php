<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <title>Oaks Residences</title>

    <!--Materialize Icons-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Favicons-->
    <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="resources/images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


</head>

<body class="ho_background">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->
    <div id="loginMain">
      <div id="">
        <div class="row">
          <div class="logo-login">
            <img src="../resources/images/oaks-logo-login.png" class="resize"/>
          </div>
          <div class="userpass">
            <form style="position: relative; z-index: 30;">
              <div class="row">
                <div class="input-field col s12">
                  <input id="username" type="text">
                  <label for="username" style="color: #000;">Username</label>
                </div>
                <div class="input-field col s12">
                  <input id="password" type="password">
                  <label for="password" style="color: #000;">Password</label>
                </div>
                <div class="center">
                  <a class="waves-effect waves-light btn" style="background-color:#DB9501; width: 50%; text-align: center;">Login</a>
                </div>
                <div>
                  <a href="#"><p style="text-align: center; color: #000;">Forgot password</p></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- ================================================Scripts================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="resources/js/plugins/jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="resources/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="resources/js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="resources/js/custom-script.js"></script>

    <!-- <script type="text/javascript">
      $(document).ready(function(){
        $('.slider').slider();
      });
    </script> -->
</body>

</html>
