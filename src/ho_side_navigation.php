<!--This html file is for the sidebar panel-->

<div class="wrapper">

    <!-- START LEFT SIDEBAR NAV-->
    <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="user-details cyan darken-2">
        <div class="row">
            <div class="col col s4 m4 l4">
                <img src="/resources/images/avatar.jpg" alt="" class="circle resize2">
            </div>
            <div class="col col s8 m8 l8">
                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">John Doe</a>
                <p class="user-roal">#7 Dickens</p>
            </div>
        </div>
        </li>
        <li class="bold"><a href="index.html" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
        </li>
        <li><a href="../views/visitors.php"><i class="material-icons">directions_walk</i> Visitors</a></li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="material-icons">payments</i> Payments</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="/views/monthly_dues.php">Monthly Dues</a>
                            </li>
                            <li><a href="/views/parkingFee.php">Parking Fees</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="material-icons">comment</i> Posts</a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="/views/posts.php">View All Posts</a>
                            </li>
                            <li><a href="/views/manage_posts.php">Manage Posts</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <li class="li-hover"><div class="divider"></div></li>
        <li><a href="../views/settings.php"><i class="mdi-action-settings"></i> Settings</a></li>
        <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a></li>
    </ul>
        <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only #DB9501 lighten-1"><i class="mdi-navigation-menu"></i></a>
    </aside>
    <!-- END LEFT SIDEBAR NAV-->

    <!-- //////////////////////////////////////////////////////////////////////////// -->



<!-- END WRAPPER -->
