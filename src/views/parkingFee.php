<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>

                  <div class="col s12 m12 l12">
                    <div class="card-panel">
                      <div class="">
                          <div class="nav-wrapper">
                            <form>
                              <div class="input-field">
                                <i class="mdi-action-search prefix input-field"></i>
                                <input id="icon_prefix" type="text">
                                <label for="icon_prefix">Search</label>
                              </div>
                            </form>
                          </div>
                        <table class="bordered highlight centered">
                          <thead>
                            <tr>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>House #</th>
                              <th>Street</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Alvin</td>
                              <td>Eclair</td>
                              <td>87</td>
                              <td>Dickens</td>
                              <td><a class="waves-effect waves-light btn modal-trigger" style="margin: 5px; background-color: #6E6702;" href="#billingModal"><i class="material-icons left">receipt</i>View</a></td>
                            </tr>
                            <tr>
                              <td>Alano</td>
                              <td>Shannel</td>
                              <td>35</td>
                              <td>Mansfield</td>
                              <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">receipt</i>View</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!--START OF BILL MODAL-->
                  <div id="billingModal" class="modal">
                    <div class="modal-content">
                      <h5>Eclair, Alvin Angelo T.</h5> <!--Name of home owner-->
                      <div class="divider"></div>
                      <p>#87 Dickens St. <br> <!--Address of home owner-->
                      09152462389</p><!--Contact Number of home owner-->
                    </div> <!--TABLE FOR BILL-->
                      <table class="bordered centered">
                        <thead>
                          <th>Previous Balance</th>
                          <th>Current Charges</th>
                          <th>Total Amount Due</th>
                          <th>Due Date</th>
                        </thead>
                        <tbody>
                          <td>0.00</td>
                          <td>200.00</td>
                          <td>200.00<td>
                          <td>2/15/2018</td>
                        </tbody>
                      </table>
                    <div style="margin: 20px;">
                      <div class="row">
                        <h5><span style"background-color: #B7AFAF;">Billing Summary</span></h5>
                        <div class="divider"></div>
                          <table style="margin: 10px;">
                            <tbody>
                              <tr>
                                <td>Parking Fee</td>
                                <td>200.00</td>
                              </tr>
                              <tr>
                                <td>Previous Balance</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td> </td>
                                <td> </td>
                              </tr>
                              <tr>
                                <td><span style="font-weight: bold;">Total Amount Due</span></td>
                                <td><span style="font-weight: bold;">200.00</span></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #B7AFAF;">Close</a>
                      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #B7AFAF;">Print</a>
                      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #108DC9;">Pay</a>
                    </div>
                  </div>
          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });

          </script>
      </body>

      </html>
