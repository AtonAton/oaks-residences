<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Oaks Residences</title>

    <!--Materialize Icons-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Favicons-->
    <link rel="icon" href="../resources/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="../resources/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


</head>

<body>
    <header id="header" class="page-topbar">
      <?php include '../ho_header_topbar.php';?>
    </header>
    <div id="main">
      <!-- <div class="card-panel"> -->
      <div class="main s12 m7 l4 post_page">
        <div id="row">
          <!--Start of Posts-->
                <div class="card row col s12 m12 l12" style="height: 100px;">
                  <div class="col s2 m2 l2" style="background-color: #DB9501; color: #FFF; height: 100px;">
                    <p class="center" style="font-size: 40px;">27</p>
                    <p class="center" style="margin-top: 5px;">Feb 2018</p>
                  </div>
                  <div class="col s8 m8 l8">
                      <p>Title</p>
                      <p class="desc">Posted by: admin</p>
                  </div>
                  <div class="col s2 m2 l2">
                      <a target="_blank" href="postsMain.php?source=view_post&id=<?= $post_id ;?>" class="modal-action modal-close waves-effect waves-green btn" style="margin-top: 30px; background-color: #6E6702;">View Post</a>
                  </div>
                 </div>

                    <div class="col s12 m7 l4">
                      <div class="card horizontal opacity">
                        <div class="card-action">
                          <ul id="horizontal-list">
                            <li><a class="btn-floating halfway-fab waves-effect waves-light blue"><i class="material-icons">grade</i></a></li>
                            <li><span style="font-weight: Bold; font-size: 20px;">Announcement</span></li>
                            <li style="float: right;"><p style="font-style: Bold; font-size: 15px; background-color: #C05805">1/5/2018</p></li>
                          </ul>
                        </div>
                        <div class="divider"></div>
                        <div class="card-stacked">
                          <div class="card-content" style="background: rgba(255,255,255,0.7); color: #000;">
                            <p>Announcement: The village swimming pool will is under renovation. In line with this, it will be closed from January 15 - January 30.
                            Please do not hesistate to contact us for further concerns. Thank you for your consideration.</p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col s12 m7 l4">
                      <div class="card horizontal opacity">
                        <div class="card-action" style="backgorund-color: #001eff">
                          <ul id="horizontal-list">
                            <li><a class="btn-floating halfway-fab waves-effect waves-light green"><i class="material-icons">grade</i></a></li>
                            <li><span style="font-weight: Bold; font-size: 20px;">Reminder</span></li>
                            <li style="float: right;"><p style="font-style: Bold; font-size: 15px; background-color: #C05805">1/10/2018</p></li>
                          </ul>
                        </div>
                        <div class="divider"></div>
                        <div class="card-stacked">
                          <div class="card-content" style="background: rgba(255,255,255,0.7); color: #000;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col s12 m7 l4">
                      <div class="card horizontal opacity">
                        <div class="card-action" style="backgorund-color: #001eff">
                          <ul id="horizontal-list">
                            <li><a class="btn-floating halfway-fab waves-effect waves-light yellow"><i class="material-icons">grade</i></a></li>
                            <li><span style="font-weight: Bold; font-size: 20px;">Promotion</span></li>
                            <li style="float: right;"><p style="font-style: Bold; font-size: 15px; background-color: #C05805">1/14/2018</p></li>
                          </ul>
                        </div>
                        <div class="divider"></div>
                        <div class="card-stacked">
                          <div class="card-content" style="background: rgba(255,255,255,0.7); color: #000;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                          </div>
                        </div>
                      </div>
                    </div>
          <!--End of Posts-->
      </div>
    </div>
  <!-- </div> -->
    <!-- ================================================Scripts================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../resources/js/custom-script.js"></script>

    <!--Script for modals-->
    <script type="text/javascript">

      $(document).ready(function(){
        $('.modal').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            inDuration: 300, // Transition in duration
            outDuration: 200, // Transition out duration
            startingTop: '1%', // Starting top style attribute
            endingTop: '10%', // Ending top style attribute
            ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
              alert("Ready");
              console.log(modal, trigger);
            },
            complete: function() { alert('Closed'); } // Callback for Modal close
          }
        );

      });

    </script>
</body>

</html>
