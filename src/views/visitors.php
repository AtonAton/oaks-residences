<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div style="">
            <?php include '../side_navigation.php';?>
          </div>
          <div id="main">


            <div class="hide-on-small-only">
                <div class="col s12 m12 l12" style="margin-left: 30px;">
                    <h5 class="breadcrumbs-title" style="color: #C05805;">Visitors</h5>
                    <ol class="breadcrumbs">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Visitors</li>
                    </ol>
                </div>
            </div>
            <div class="card-panel">
              <div class="row">


                  <!--Start of Data Table for Visitor log-->
                    <div class="row col s12 m12 l12" id="Logs">
                        <!--Start of Search Bar && Add Admin Button-->
                        <div class="input-field">
                             <i class="mdi-action-search prefix input-field"></i>
                             <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                             <label for="icon_prefix">Search</label>
                        </div>
                        <!--End of Search bar && Add Admin Button-->
                        <div class="divider"></div>
                        <!--Start of Data Table for Current Visitors-->
                        <table class="bordered highlight responsive-table centered" id="currentVisitorsTable">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Purpose</th>
                              <th>Date In</th>
                              <th>Time In</th>
                              <th>Date Out</th>
                              <th>Time Out</th>
                              <th>Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Hazel Que</td>
                              <td>Visit</td>
                              <td>03/01/2017</td>
                              <td>10:32</td>
                              <td>03/01/2017</td>
                              <td>15:47</td>
                              <td>Single</td>
                            </tr>
                            <tr>
                              <td>Hazel Que</td>
                              <td>Visit</td>
                              <td>03/01/2017</td>
                              <td>10:32</td>
                              <td>03/01/2017</td>
                              <td>15:47</td>
                              <td>Single</td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
              </div>
          </div>
        <!--Visit Modal End-->

          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
      </body>

      </html>
