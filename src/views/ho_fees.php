<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
        <!-- Start Page Loading -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <!-- End Page Loading -->
          <header id="header" class="page-topbar">
            <?php include '../ho_header_topbar.php';?>
          </header>
          <div>
            <!--Start of Wrapper-->
            <div class="">
              <div class="card-content">
                <p></p>
              </div>
              <div class="card-tabs">
                <ul class="tabs tabs-fixed-width">
                  <li class="tab"><a class="active" href="#monthly">Monthly Dues</a></li>
                  <li class="tab"><a href="#parking">Parking Fees</a></li>
                </ul>
              </div>
              <!--Start of Content-->
              <div class="card-content grey lighten-4">
                <!--Start of Monthly Dues table-->
                <div id="monthly">
                  <div class="row">
                    <!--Start of Monthly Payment Cards-->
                    <div class="card">
                      <div class="row"><!--Wrapper-->
                        <div class="current-bill">
                          <h5 style="font-style: italic; font-size: 20px; padding-bottom: 1px; margin-top: 0px;">February 5, 2018</h5>
                        </div>
                        <div>
                          <table id="bill">
                            <tbody>
                              <tr>
                                <td>Current Charges</td>
                                <td>850.00</td>
                              </tr>
                              <tr>
                                <td>Previous Balance</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td>Overdue Penalties</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td>Credit</td>
                                <td>850.00</td>
                              </tr>
                              <tr>
                                <td>Total Charges</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Ammount Paid</td>
                                <td>0.00</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="row"><!--Wrapper-->
                        <div class="past-bill">
                          <h5 style="font-style: italic; font-size: 20px; padding-bottom: 1px; margin-top: 0px;">January 5, 2018</h5>
                        </div>
                        <div>
                          <table id="bill">
                            <tbody>
                              <tr>
                                <td>Current Charges</td>
                                <td>850.00</td>
                              </tr>
                              <tr>
                                <td>Previous Balance</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td>Overdue Penalties</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td>Credit</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td>Total Charges</td>
                                <td>850.00</td>
                              </tr>
                              <tr>
                                <td></td>
                                <td></td>
                              </tr>
                              <tr>
                                <td>Ammount Paid</td>
                                <td>1,700.00</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                    <!--End of Monthly Payment Cards-->
                  </div>
                </div>
                <!--End of Monthly Dues table-->

                <!--Start of Parking Fees table-->
                <div id="parking">

                </div>
                <!--End of Parking Fees table-->
              </div>
              <!--End of Content-->
            </div>
            <!--End of Wrapper-->
          </div>

          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
      </body>

      </html>
