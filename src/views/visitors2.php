<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="wrapper">
<!--Breadcrumb-->
                <div class="col s12 m12 l12" style="margin-left: 30px;">
                    <h5 class="breadcrumbs-title" style="color: #C05805;">Visitors</h5>
                    <ol class="breadcrumbs">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Visitors</li>
                    </ol>
                </div>
<!--Breadcrumb-->


<!--1st Layer of tabs-->
                <div class="card">
                  <div class="row">
                    <div class="col s12">
                      <ul class="tabs tab-demo-active z-depth-1" style="width: 100%; background-color: #C05805;">
                        <li class="tab col s3" style="margin-top: 15px;"><a class="white-text waves-effect waves-light active" href="#HomeOwners">Home Owners</a>
                        </li>
                        <li class="tab col s3" style="margin-top: 15px;"><a class="white-text waves-effect waves-light" href="#VisitorsLog">Visitors Log</a>
                        </li>
                        <li class="tab col s3" style="margin-top: 15px;"><a class="white-text waves-effect waves-light" href="#vestibulum">Security</a>
                        </li>
                      <div class="indicator" style="right: 513px; left: 0px;"></div><div class="indicator" style="right: 513px; left: 0px;"></div></ul>
                    </div>
                    <div class="col s12">
                      <div id="HomeOwners" class="col s12" style="display: block;">
                        <!--Start of Search Bar-->
                        <div class="input-field">
                          <i class="mdi-action-search prefix input-field"></i>
                          <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                          <label for="icon_prefix">Search</label>
                        </div>
                        <!--End of Search bar-->
                        <div class="divider"></div>
                        <!--Start of Data Table-->
                        <table class="bordered highlight col s12 m12 l12 centered" id="myTable">
                          <thead>
                            <tr>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>House #</th>
                              <th>Street</th>
                              <th>Contact No.</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Ma. Teresa</td>
                              <td>Loya</td>
                              <td>47</td>
                              <td>Dickens</td>
                              <td>09225637849</td>
                              <td><a class="waves-effect waves-light btn modal-trigger" style="margin: 5px; background-color: #6E6702;" href="#">Ban A Visitor</a></td>
                            </tr>
                            <tr>
                              <td>Jane Michelle</td>
                              <td>Sarmiento</td>
                              <td>9</td>
                              <td>Mansfield</td>
                              <td>09164515761</td>
                              <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;" href="#">Ban A Visitor</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div id="VisitorsLog" class="col s12" style="display: none;">
                      <div class="col s12 m12 l12">
                        <!--Start of Search Bar-->
                        <div class="input-field">
                          <i class="mdi-action-search prefix input-field"></i>
                          <input id="icon_prefix visitorsLog" onkeyup="searchVisitorsLog()" type="text">
                          <label for="icon_prefix" class="">Search</label>
                        </div>
                        <!--End of Search Bar-->
                        <div class="divider"></div>
                        <!--Start of Data Table for Visitor log-->
                          <table class="bordered highlight col s12 m12 l12 centered" id="visitorsLogTable" style="padding: 0px;">
                            <thead>
                              <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Home Owner</th>
                                <th>Purpose</th>
                                <th>Relationship</th>
                                <th>Date In</th>
                                <th>Time In</th>
                                <th>Date Out</th>
                                <th>Time Out</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Hannah</td>
                                <td>Yamaguchi</td>
                                <td>Aton Layno</td>
                                <td>Birthday</td>
                                <td>Cousin</td>
                                <td>February 21, 2018</td>
                                <td>5:34 pm</td>
                                <td>February 21, 2018</td>
                                <td>5:35 pm</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                    <div id="vestibulum" class="col s12" style="display: none;">
                      <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                        <li>
                          <div class="collapsible-header active center" style="background-color: #DB9501; color: #FFF;">People</div>
                          <div class="collapsible-body">
                            <div class="wrapper" style="padding: 30px;">
                              <!--Start of Search Bar && Add Admin Button-->
                              <div class="input-field">
                                <i class="mdi-action-search prefix input-field"></i>
                                <input id="icon_prefix person" onkeyup="searchQueue()" type="text">
                                <label for="icon_prefix">Search</label>
                              </div>
                              <!--End of Search bar && Add Admin Button-->
                              <div class="divider"></div>
                              <!--Start of Data Table for Current Visitors-->
                              <table class="bordered highlight col s12 m12 l12 centered" id="searchQueueTable">
                                <thead>
                                  <tr>
                                    <th class="sortable">First Name</th>
                                    <th class="sortable">Last Name</th>
                                    <th class="sortable">Gender</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr class="visible">
                                    <td class="odd">Jhonel</td>
                                    <td class="odd">Reyesnalang</td>
                                    <td class="odd">Female</td>
                                    <td class="odd">
                                      <a onclick="javascript: return confirm('Are you sure you want to unbanned this person?')" href="visitors_controller.php?source=logs&amp;update=1" class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;">Lift</a>
                                    </td>
                                  </tr>

                                  <tr class="visible">
                                    <td>Ralph</td>
                                    <td>Rodrigo</td>
                                    <td>Male</td>
                                    <td>
                                      <a onclick="javascript: return confirm('Are you sure you want to unbanned this person?')" href="visitors_controller.php?source=logs&amp;update=2" class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;">Lift</a>
                                    </td>
                                  </tr>

                                  <tr class="visible">
                                    <td class="odd"></td>
                                    <td class="odd"></td>
                                    <td class="odd">Male</td>
                                    <td class="odd">
                                      <a onclick="javascript: return confirm('Are you sure you want to unbanned this person?')" href="visitors_controller.php?source=logs&amp;update=4" class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;">Lift</a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                                <hr>
                                  <ul class="pagination center" style="margin-top: 10px;">
                                    <li class="disabled"><a href="#"><i class="mdi-navigation-chevron-left"></i></a></li><li class="waves-effect"><a class="active" href="visitors_controller.php?source=logs&amp;page=1#banned">1</a></li><li class="disabled"><a href="#"><i class="mdi-navigation-chevron-right"></i></a></li>                </ul>

                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="collapsible-header center" style="background-color: #DB9501; color: #FFF;">Vehicles</div>
                          <div class="collapsible-body">
                            <div class="warpper" style="padding: 30px;">
                                  <!--Start of Search Bar && Add Admin Button-->
                                  <div class="input-field">
                                    <i class="mdi-action-search prefix input-field"></i>
                                    <input id="icon_prefix plate" onkeyup="searchPlateNum()" type="text">
                                    <label for="icon_prefix">Search</label>

                                  </div>
                                  <!--End of Search bar && Add Admin Button-->
                                  <div class="divider"></div>
                                  <!--Start of Data Table for Current Visitors-->
                                    <table class="bordered highlight col s12 m12 l12 centered" id="searchVehicleTable">
                                      <thead>
                                            <tr>
                                              <th class="sortable">Model</th>
                                              <th class="sortable">Color</th>
                                              <th class="sortable">Plate No.</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <tr class="visible">
                                              <td>Honda City</td>
                                              <td>White</td>
                                              <td>UNO-5432</td>
                                              <td>
                                               <a onclick="javascript: return confirm('Are you sure you want to unbanned this person?')" href="visitors_controller.php?source=logs&amp;update=2" class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;">Lift</a>
                                              </td>
                                            </tr>
                                          </tbody>
                                    </table>
                                    <hr>
                                         <ul class="pagination center" style="margin-top: 10px;">
                                              <li class="disabled"><a href="#"><i class="mdi-navigation-chevron-left"></i></a></li><li class="waves-effect"><a class="active" href="visitors_controller.php?source=logs&amp;page=1#banned">1</a></li><li class="disabled"><a href="#"><i class="mdi-navigation-chevron-right"></i></a></li>                </ul>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>

            </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>

  </body>
</html>
