<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="card-panel">
              <div class="row col s12 m12 l12">
                    <div class="card-panel col s12 m12 l12">
                      <!--Start of Search Bar && Add Streets Button-->
                      <div class="input-field">
                        <i class="mdi-action-search prefix input-field"></i>
                        <input id="icon_prefix" type="text">
                        <label for="icon_prefix">Search</label>
                        <a class="waves-effect waves-light btn modal-trigger" href="../views/add_house.php" style="margin-left: 180px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add House</a>
                      </div>
                      <!--End of Search bar && Add Streets Button-->
                      <div class="divider"></div>
                      <!--Start of Data Table for Streets-->
                        <table class="bordered highlight col s12 m12 l12 centered">
                          <thead>
                            <tr>
                              <th>House ID</th>
                              <th>House #</th>
                              <th>Street Name</th>
                              <th>Current Owner</th>
                              <th>Date Occupied</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>PJK256XC77</td>
                              <td>1</td>
                              <td>Dickens</td>
                              <td>Juan M. Pablo</td>
                              <td>10/17/1993</td>
                            </tr>
                            <tr>
                              <td>2884EA89CF</td>
                              <td>2</td>
                              <td>Dickens</td>
                              <td>Virginia C. Aguila</td>
                              <td>7/12/1997</td>
                            </tr>
                            <tr>
                              <td>6A4CB7BC83</td>
                              <td>3</td>
                              <td>Dickens</td>
                              <td>Adolfo V. Guerrero</td>
                              <td>4/26/1992</td>
                            </tr>
                            <tr>
                              <td>CB171AACAC</td>
                              <td>4</td>
                              <td>Dickens</td>
                              <td>Joyce M. Solano</td>
                              <td>1/9/1995</td>
                            </tr>
                            <tr>
                              <td>37D5907EE8</td>
                              <td>5</td>
                              <td>Dickens</td>
                              <td></td>
                              <td></td>
                            </tr>
                            <tr>
                              <td>46494EACAA</td>
                              <td>6</td>
                              <td>Dickens</td>
                              <td>Immanuel T. Agbuya</td>
                              <td>3/15/1998</td>
                            </tr>
                            <tr>
                              <td>E92C458534</td>
                              <td>7</td>
                              <td>Dickens</td>
                              <td></td>
                              <td></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
                  <!--End of Data Table for Streets-->

          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
      </body>

      </html>
