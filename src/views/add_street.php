<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="row col s12 m12 l12">
                <!--Start of Add Street Panel-->
                <div class="card-panel">
                  <div class="row">
                      <div class="nav-wrapper">
                        <h5 style="margin: 0px;">Street Details</h5>
                        <div class="divider" style="margin-left: -20px; margin-right: -20px;"></div>
                      </div>
                      <form>
                        <table>
                          <tbody>
                            <tr><!--Start of street name-->
                              <td><p style="font-weight: bold;">Street Name:</p></td>
                              <td>
                                <div>
                                  <div class="input-field inline">
                                    <input id="streetName" type="text" class="validate">
                                    <label for="streetName" data-error=""></label>
                                  </div>
                                </div>
                              </td>
                            </tr><!--End of street name-->
                            <tr><!--Start of num of houses-->
                              <td><p style="font-weight: bold;">Number of Houses:</p></td>
                              <td>
                                <div>
                                  <div class="input-field inline">
                                    <input id="numOfHouses" type="number" min="1" class="validate">
                                    <label for="numOfHouses" data-error="Value unacceptable."></label>
                                  </div>
                                  <p class="desc">The total number of houses for this street.</p>
                                </div>
                              </td>
                            </tr><!--End of num of houses-->
                          </tbody>
                        </table>
                      </form>
                  </div>
                  <div class="footer">
                    <a href="#verifyModal" class="waves-effect waves-light btn modal-trigger waves-green" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">send</i>Submit</a>
                  </div>
                <!--End of Add Street Panel-->

          </div>
          <!--Start of Add Street Panel-->
          <div id="verifyModal" class="modal">
            <div class="modal-content">
              <p>To continue, kindly provide your password</p>
              <form>
                <div class="input-field">
                  <input id="penalty" type="password" class="validate">
                  <label for="penalty" data-error="Value unacceptable."></label>
                </div>
              </form>
              <div class="footer">
                <a href="../views/streets.php" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">send</i>Submit</a>
              </div>
            </div>
          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
      </body>

      </html>
