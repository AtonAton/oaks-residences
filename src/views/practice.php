<?php

 bridgeURL();
  
?>

<div class="col s12 m12 l12" style="margin-left: 30px;">
    <h5 class="breadcrumbs-title" style="color: #C05805;">Monthly Dues</h5>
    <ol class="breadcrumbs">
        <li><a href="index.php">Dashboard</a></li>
        <li><a href="#">Payment</a></li>
        <li class="active">Monthly Dues</li>
    </ol>
</div>

<div class="card-panel" style="margin:30px;">
  <div class="row col s12 m12 l12">
      <div class="card col s12 m12 l12">

      <!-- <div class="card-panel"> -->
            <div class="nav-wrapper">
                 <div class="input-field">
                      <i class="mdi-action-search prefix input-field"></i>
                      <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                      <label for="icon_prefix">Search</label>
                </div>
            </div>
						<div class="divider"></div>
          <table class="centered bordered highlight" id="mdue" >
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
								<th>Street</th>
								<th>House #</th>
              </tr>
            </thead>
            <tbody>
                 <tr>
									 <td>Aton</td>
									 <td>Layno</td>
									 <td>Oaks Rd.</td>
									 <td>2</td>
									 <td><a class='waves-effect waves-light btn modal-trigger' href='dues.php?source=viewBill&id={$account_id}' style='background-color: #6E6702;'><i class='material-icons left'>visibility</i>View</a></td>
								 </tr>
						</tbody>
          </table>
          <ul class="pagination center" style="margin-top: 10px;">

                <?php

                  if($page == 1 || $page == 0){

                                    echo "<li class='disabled'><a href='#'><i class='mdi-navigation-chevron-left'></i></a></li>";

                                }else{

                                    echo "<li class='waves-effect'><a href='dues.php?source=mdues_accounts&page=".($page-1)."'><i class='mdi-navigation-chevron-left'></i></a></li>";

                                }


                                for($i = 1; $i <= $total_pages; $i++){

                                    if($i == $page){

                                        echo "<li class='waves-effect'><a class='active' href='dues.php?source=mdues_accounts&page={$i}'>{$i}</a></li>";

                                    }else{

                                        echo "<li class='waves-effect'><a href='dues.php?source=mdues_accounts&page={$i}'>{$i}</a></li>";

                                    }

                                }


                                if($page == $total_pages){

                                    echo"<li class='disabled'><a href='#'><i class='mdi-navigation-chevron-right'></i></a></li>";

                                }else{
                                    echo"<li class='waves-effect'><a href='dues.php?source=mdues_accounts&page=".($page+1)."'><i class='mdi-navigation-chevron-right'></i></a></li>";
                                }

                ?>

              </ul>
      </div>
  </div>
</div>

 <script>
    function searchFilter() {
      // Declare variables
      var input, filter, table, tr, td, i, j;
      input = document.getElementById("icon_prefix search");
      filter = input.value.toUpperCase();
      table = document.getElementById("mdue");
      tr = table.getElementsByTagName("tr");

      // Loop through all table rows, and hide those who don't match the search query

        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          if (td) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }
        }

    }
</script>
<!--
<?php
		if(isset($_GET['page'])){

				$page = $_GET['page'];

				 }else{
					 $page = "";
				 }


				 if($page == "" || $page == 1){
							 $page_1 = 0;
				 }else{
					 $page_1 = ($page * 5) - 5;
				 }

				 $user_query_count = "SELECT * FROM homeowners_tbl";
				 $find_count = mysqli_query($connection, $user_query_count);
				 $count = mysqli_num_rows($find_count);
				 $total_pages = ceil($count / 5);

					monthly_due($page_1);

 ?> -->
