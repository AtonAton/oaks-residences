<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>

                  <div class="col s12 m12 l12 card-panel">

                  <!--START OF Receipt-->
                  <div id="bill" class="row">
                    <!--HEADER START -->
                    <div class="modal-content">
                      <div class="center">
                        <img src="../resources/images/oaks-logo-login.png">
                      </div>
                      <div class="center sub">
                        <h4>Oaks Residences</h4>
                        <p style="text-align: center;">Sitio Munting Dilao, San Isidro, Cainta, Lalawigan ng Rizal<br>
                          Telephone: +632 570-6186 | Fax: +632 439-0933 | Mobile: +63 908-810-5227
                        </p>
                      </div>
                      <div class="center">
                        <h3>OFFICIAL RECEIPT</h3>
                      </div>
                    </div>
                    <!--HEADER END -->
                    <!--CONTENT START -->
                    <div class="divider"></div>
                    <div>
                      <table style="margin: 0px; padding: 0px;">
                        <tbody>
                          <tr>
                            <td>Received from:</td>
                            <td>Eclair, Alvin Angelo T.</td>
                          </tr>
                          <tr>
                            <td>Address:</td>
                            <td>#87 Dickens St.</td>
                          </tr>
                          <tr>
                            <td>Due Date:</td>
                            <td>02/5/2017</td>
                          </tr>
                          <tr>
                            <td>Date of Payment:</td>
                            <td>02/9/2017</td>
                          </tr>
                          <tr>
                            <td>Invoice #:</td>
                            <td>04873856-XQ</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="divider"></div>
                    <div style="margin: 20px;">
                      <div class="row" style="border-style: solid;border-width: 1px;">
                        <div style="background-color: #B7AFAF; padding: 2px; border-style: hidden hidden solid hidden; border-width: 1px;">
                          <h5 style="margin-left: 10px;">Billing Summary</h5>
                        </div>
                          <table style="margin: 10px;">
                            <tbody>
                              <tr>
                                <td>Current Charges</td>
                                <td>850.00</td>
                              </tr>
                              <tr>
                                <td>Previous Balance</td>
                                <td>850.00</td>
                              </tr>
                              <tr>
                                <td>Overdue Penalty</td>
                                <td>57.50</td>
                              </tr>
                              <tr>
                                <td> </td>
                                <td> </td>
                              </tr>
                              <tr>
                                <td>Total Charges</td>
                                <td>1,757.50</td>
                              </tr>
                              <tr>
                                <td>Credit</td>
                                <td>0.00</td>
                              </tr>
                              <tr>
                                <td> </td>
                                <td> </td>
                              </tr>
                              <tr>
                                <td><span style="font-weight: bold;">Total Amount Due</span></td>
                                <td><span style="font-weight: bold;">1,757.50</span></td>
                              </tr>
                              <tr>
                                <td>Amount Paid</td>
                                <td><span style="font-weight: bold;">2,000.00</span></td>
                              </tr>
                              <tr>
                                <td> </td>
                                <td> </td>
                              </tr>
                              <tr>
                                <td>Change</td>
                                <td><span style="font-weight: bold;">242.50</span></td>
                              </tr>
                            </tbody>
                          </table>
                          <div class="divider"></div>
                        </div>
                    </div>
                    <div class="center">
                      <p>***NOTE: Surpluses will reflect on your next invoice.***</p>
                    </div>
                    <div class="modal-footer">
                      <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #B7AFAF;">Print</a>
                      <a href="../views/monthly_dues.php" class="modal-action modal-close waves-effect waves-green btn modal-trigger"style="margin: 5px; background-color: #108DC9;">Confirm</a>
                    </div>
                  </div>
          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>
      </body>

      </html>
