<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <!-- <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,"> -->
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">

          <style type="text/css">
          .input-field div.error{
            position: relative;
            top: -1rem;
            left: 0rem;
            font-size: 0.8rem;
            color: red;
            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            -o-transform: translateY(0%);
            transform: translateY(0%);
          }
          .input-field label.active{
              width:100%;
          }
          .left-alert input[type=text] + label:after,
          .left-alert input[type=password] + label:after,
          .left-alert input[type=email] + label:after,
          .left-alert input[type=url] + label:after,
          .left-alert input[type=time] + label:after,
          .left-alert input[type=date] + label:after,
          .left-alert input[type=datetime-local] + label:after,
          .left-alert input[type=tel] + label:after,
          .left-alert input[type=number] + label:after,
          .left-alert input[type=search] + label:after,
          .left-alert textarea.materialize-textarea + label:after{
              left:0px;
          }
          .right-alert input[type=text] + label:after,
          .right-alert input[type=password] + label:after,
          .right-alert input[type=email] + label:after,
          .right-alert input[type=url] + label:after,
          .right-alert input[type=time] + label:after,
          .right-alert input[type=date] + label:after,
          .right-alert input[type=datetime-local] + label:after,
          .right-alert input[type=tel] + label:after,
          .right-alert input[type=number] + label:after,
          .right-alert input[type=search] + label:after,
          .right-alert textarea.materialize-textarea + label:after{
              right:70px;
          }
          </style>

      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
            <div class="wrapper">
              <div class="card" style="margin: 30px;">
                <!--Start of Add Admin Modal-->
                <div id="addAdminModal" class="row">
                  <div class="row">
                    <form id="formValidate" class="col s12 left-alert formValidate">
                      <div class="row">
                        <div class="row"><!--Start of Personal Details Div-->
                          <div style="background-color: #d6d6d6; padding: 5px;" class="col s12 m12 l12">
                          <h5 style="margin: 20px;color: #C05805;">Personal Details</h5> <!--Personal Details Fields-->
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="First Name" id="first_name" name="first_name" type="text" class="validate">
                            <label for="first_name">First Name</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Middle Name" id="middle_name" name="middle_name" type="text" class="validate">
                            <label for="middle_name">Middle Name</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Last Name" id="last_name" name="last_name" type="text" class="validate">
                            <label for="last_name">Last Name</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Phone" id="phone" name="phone"type="text" class="validate">
                            <label for="phone">Phone</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Mobile" id="mobile" name="mobile" type="text" class="validate">
                            <label for="mobile">Mobile</label>
                          </div>
                          <div class="input-field col s12">
                            <select name="street">
                              <option value="" disabled selected>Choose street</option>
                              <option value="1">Option 1</option>
                              <option value="2">Option 2</option>
                              <option value="3">Option 3</option>
                            </select>
                            <label>Street</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <select name="houseNum" id="houseNum">
                              <option value="" selected>Choose house number</option>
                              <option value="2">Option 1</option>
                              <option value="3">Option 2</option>
                              <option value="4">Option 3</option>
                            </select>
                            <label>House Number</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <div>
                              <p style="font-size: 15px;">Gender
                                <input name="gender" type="radio" id="test1" />
                                <label for="test1">Male</label>
                                <input name="gender" type="radio" id="test2" />
                                <label for="test2">Female</label>
                              </p>
                            </div>
                          </div>
                          <div class="col s12 m12 l12">
                            <p style="font-size: 15px">Birth Date</p>
                            <input type="text" class="datepicker" id="birthdate">
                          </div>
                        </div><!--End of Personal Details Div-->
                        <div class="row"><!--Start of Alternate Contact Div-->
                          <div style="background-color: #d6d6d6; padding: 5px;">
                          <h5 style="margin: 20px; color: #C05805; ">Alternate Contact</h5> <!--Personal Details Fields-->
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="First Name" id="first_name" type="text" class="">
                            <label for="first_name">First Name</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Middle Name" id="middle_name" type="text" class="validate">
                            <label for="middle_name">Middle Name</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Last Name" id="last_name" type="text" class="validate">
                            <label for="last_name">Last Name</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Phone" id="phone" type="text" class="validate">
                            <label for="phone">Phone</label>
                          </div>
                          <div class="input-field col s12 m12 l12">
                            <input placeholder="Mobile" id="mobile" type="text" class="validate">
                            <label for="mobile">Mobile</label>
                          </div>
                        </div><!--End of Alternate Contact Div-->
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                            <i class="mdi-content-send right"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!--End of Add Admin Modal-->
              </div>
          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <script type="text/javascript" src="../resources/js/jQuery_validation.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">
            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 30, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
          <script>
            $.validator.setDefaults({
              highlight: function(element){
                $(element).addClass('invalid');
              },
              unhighlight: function(element){
                $(element).removeClass('valid');
              }
            });
            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z]+$/i.test(value);
              });

             $("#formValidate").validate({
                rules: {
                    first_name: {
                        required: true,
                        lettersonly: true
                    },
                    middle_name: {
                        required: true,
                        lettersonly: true
                    },
                    last_name: {
                        required: true,
                        lettersonly: true
                    },
                    phone: {
                        maxlength: 7,
                        number: true
                    },
                    mobile: {
                        maxlength: 11,
                        number: true
                    },
                    street: {
                        min: 1
                    },
                    houseNum: {
                        min: 1
                    },
                    gender:"required",
                    },
                //For custom messages
                messages: {
                    first_name:{
                        lettersonly: "Invalid name."
                    },
                    middle_name:{
                        lettersonly: "Invalid name."
                    },
                    last_name:{
                        lettersonly: "Invalid name."
                    },
                },

                errorElement : 'div',
                errorPlacement: function(error, element) {
                  var placement = $(element).data('error');
                  if (element.attr("type") == "radio") {
                      error.insertBefore(element);
                  } else {
                      error.insertAfter(element);
                  }
                  // if (placement) {
                  //   $(placement).append(error)
                  // } else {
                  //   error.insertAfter(element);
                  // }
                }
             });
          </script>

      </body>

      </html>
