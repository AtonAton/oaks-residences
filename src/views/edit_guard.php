<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="wrapper">
<!--Breadcrumb-->
                <div class="col s12 m12 l12" style="margin-left: 30px;">
                    <h5 class="breadcrumbs-title" style="color: #C05805;">Edit Guard</h5>
                    <ol class="breadcrumbs">
                        <li><a href="index.php">Dashboard</a></li>
												<li><a href="#">Users</a></li>
												<li><a href="index.php">Guards</a></li>
                        <li class="active">Edit Guard</li>
                    </ol>
                </div>
<!--Breadcrumb-->
								<div>
									<div class="card-panel col s12 m12 l12">
										<form action="includes/add_security_guard_exec.php" method="post" enctype="multipart/form-data">
											<div class="row col s12 m12 l12">
												<!--Start of Add Guard Div-->
												<div class="row" style="border: solid 1px #000;">
													<div class="row">
														<div class="row"><!--Start of Account Details div-->
								<!--=Header--><div style="background-color: #FFF2C4; padding: 5px; border-bottom: solid 1px #000;" class="col s12 m12 l12">
																<h5 style="margin: 20px;color: #C05805;">Account Details</h5> <!--Personal Details Fields-->
								<!--=Header--></div>

								<!--=Labels--><div class="col s12 m2 l2">
																<div class="col s12 m12 l12" style="padding-top: 35px; margin-left: 30px;">
																		<p>Username:</p>
																</div>
																<div class="col s12 m12 l12" style="padding-top: 37px; margin-left: 30px;">
																		<p>Change Password:</p>
																</div>
								<!--=Labels--></div>

								<!--=Inputs--><div class="col s12 m8 l8">
																<div class="input-field col s12 m12 l12">
																	<input id="username" name="username" class="validate" type="text" disabled>
																	<label for="username">Username</label>
																</div>
																<div class="input-field col s12 m12 l12">
																	<input id="password" name="password" class="validate" type="password">
																	<label for="password">Password</label>
																</div>
								<!--=Inputs--></div>

								</div><!--End of Account Details div-->


								<div class="row"> <!--End of Personal Details div-->
								<!--=Header--><div style="background-color: #FFF2C4; padding: 5px; border-top: solid 1px #000; border-bottom: solid 1px #000;" class="col s12 m12 l12">
									<h5 style="margin: 20px;color: #C05805;">Personal Details</h5> <!--Personal Details Fields-->
								<!--=Header--></div>

								<!--=Labels--><div class="col s12 m2 l2">
								<div class="col s12 m12 l12" style="padding-top: 35px; margin-left: 30px;">
										<p>First name:</p>
								</div>
								<div class="col s12 m12 l12" style="padding-top: 37px; margin-left: 30px;">
										<p>Middle name:</p>
								</div>
								<div class="col s12 m12 l12" style="padding-top: 37px;margin-left: 30px;">
										<p>Last name:</p>
								</div>
								<div class="col s12 m12 l12" style="padding-top: 37px; margin-left: 30px;">
										<p>Mobile:</p>
								</div>
								<div class="col s12 m12 l12" style="padding-top: 37px; margin-left: 30px;">
										<p>Gender:</p>
								</div>
								<div class="col s12 m12 l12" style="padding-top: 37px; margin-left: 30px;">
										<p>Picture:</p>
								</div>
								<!--=Labels--></div>

								<!--=Inputs--><div class="col s12 m8 l8">
								<div class="input-field col s12 m12 l12">
								<input id="firstname" name="firstname" class="validate" type="text">
								<label for="firstname">First Name</label>
								</div>

								<div class="input-field col s12 m12 l12">
								<input id="middlename" name="middlename" class="validate" type="text">
								<label for="middlename">Middle Name</label>
								</div>

								<div class="input-field col s12 m12 l12">
								<input id="lastname" name="lastname" class="validate" type="text">
								<label for="lastname">Last Name</label>
								</div>

								<div class="input-field col s12 m12 l12">
								<input id="mobile" name="mobile" class="validate" type="number">
								<label for="mobile">Mobile</label>
								</div>

								<div class="input-field col s12 m12 l12">
										<p>
											<input name="gender" id="male" value="Male" type="radio">
											<label for="male">Male</label>
											<input name="gender" id="female" value="Female" type="radio">
											<label for="female">Female</label>
										</p>
								</div>
								<div class="input-field col s12 m12 l12" style="padding-top: 40px;">
									<input id="image" name="image" class="validate" type="file">
									<label for="image"></label>
								</div>
								<!--=Inputs--></div>

								</div> <!--End of Personal Details div-->



								</div>
								</div>
								<div class="footer right-align">
								<button class="modal-action modal-close waves-effect waves-green btn right" style="margin: 5px; background-color: #6E6702;" type="submit" name="create_sg"><i class="material-icons left">send</i>Submit</button>
								</div>
								</div>
								<!--End of Add Admin Modal-->
								</form>
								</div>
								</div>


            </div>
						<!--wrapper end-->
					</div>
					<!--main end-->


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>

  </body>
</html>
