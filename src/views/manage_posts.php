<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="card-panel">
                <!--Start of navigation-->
                <div class="card-tabs">
                  <ul class="tabs tabs-fixed-width">
                    <li class="tab"><a class="active" href="#Announcements">Announcements</a></li>
                    <li class="tab"><a href="#Memorandums">Memorandums</a></li>
                    <li class="tab"><a href="#Reminders">Reminders</a></li>
                    <li class="tab"><a href="#Activities">Activities</a></li>
                  </ul>
                </div>
                <!--End of navigation-->
                <!--Start of contents-->
                <div class="card-content grey lighten-4">
                  <!--Start of Announcement Posts Table-->
                  <div id="Announcements" class="card-panel">
                      <div class="">
                          <div class="nav-wrapper">
                            <form>
                              <!--Start of Search bar && Add Button-->
                              <div class="input-field">
                                <i class="mdi-action-search prefix input-field"></i>
                                <input id="icon_prefix" type="text">
                                <label for="icon_prefix">Search</label>
                                <a class="waves-effect waves-light btn modal-trigger" href="../views/add_announce.php" style="margin-left: 130px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add</a>
                              </div>
                              <!--End of Search bar && Add Button-->
                              <div class="divider"></div>
                            </form>
                          </div>
                          <table class="bordered highlight col s12 m12 l12">
                            <thead>
                              <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Date Created</th>
                                <th>Created By</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Swimming Pool</td>
                                <td>Under Renovation</td>
                                <td>1/5/2018</td>
                                <td>devilarangel</td>
                                <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: Grey;" href="#" data-activates='dropdown-menu-announcement'><i class="material-icons left">arrow_drop_down</i></a></td>
                                <!-- Dropdown Structure -->
                                <ul id='dropdown-menu-announcement' class='dropdown-content'>
                                  <li><a href="#viewAnnouncementModal" class="modal-trigger"><i class="material-icons">pageview</i>View</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#editAnnouncementModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#deleteModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                                </ul>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                  </div>
                  <!--End of Announcement Posts Table-->
                  <!--Start of Memorandum Posts Table-->
                  <div id="Memorandums" class="card-panel">
                      <div class="">
                          <div class="nav-wrapper">
                            <form>
                              <!--Start of Search bar && Add Button-->
                              <div class="input-field">
                                <i class="mdi-action-search prefix input-field"></i>
                                <input id="icon_prefix" type="text">
                                <label for="icon_prefix">Search</label>
                                <a class="waves-effect waves-light btn modal-trigger" href="../views/add_memo.php" style="margin-left: 130px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add</a>
                              </div>
                              <!--End of Search bar && Add Button-->
                              <div class="divider"></div>
                            </form>
                          </div>
                        <table class="bordered highlight">
                          <thead>
                            <tr>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Date Created</th>
                              <th>Created By</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Firecrackers</td>
                              <td>Prohibited</td>
                              <td>12/20/2017</td>
                              <td>mikey007</td>
                              <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: Grey;" href="#" data-activates='dropdown-menu-memorandum'><i class="material-icons left">arrow_drop_down</i></a></td>
                              <!-- Dropdown Structure -->
                              <ul id='dropdown-menu-memorandum' class='dropdown-content'>
                                <li><a href="#viewMemorandumModal" class="modal-trigger"><i class="material-icons">pageview</i>View</a></li>
                                <li class="divider"></li>
                                <li><a href="#editMemorandumModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                <li class="divider"></li>
                                <li><a href="#deleteModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                              </ul>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!--End of Memorandum Posts Table-->
                  <!--Start of Reminder Posts Table-->
                  <div id="Reminders" class="card-panel">
                      <div class="">
                          <div class="nav-wrapper">
                            <form>
                              <!--Start of Search bar && Add Button-->
                              <div class="input-field">
                                <i class="mdi-action-search prefix input-field"></i>
                                <input id="icon_prefix" type="text">
                                <label for="icon_prefix">Search</label>
                                <a class="waves-effect waves-light btn modal-trigger" href="../views/add_reminder.php" style="margin-left: 130px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add</a>
                              </div>
                              <!--End of Search bar && Add Button-->
                              <div class="divider"></div>
                            </form>
                          </div>
                        <table class="bordered highlight">
                          <thead>
                            <tr>
                              <th>Title</th>
                              <th>Description</th>
                              <th>Date Created</th>
                              <th>Created By</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Lorem</td>
                              <td>ipsum dolor</td>
                              <td>1/10/2018</td>
                              <td>atonaton</td>
                              <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: grey;" href="#" data-activates='dropdown-menu-reminder'><i class="material-icons left">arrow_drop_down</i></a></td>
                              <!-- Dropdown Structure -->
                              <ul id='dropdown-menu-reminder' class='dropdown-content'>
                                <li><a href="#viewReminderModal" class="modal-trigger"><i class="material-icons">pageview</i>View</a></li>
                                <li class="divider"></li>
                                <li><a href="#editReminderModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                <li class="divider"></li>
                                <li><a href="#deleteModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                              </ul>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                  <!--End of Reminder Posts Table-->
                  <!--Start of Activities Posts Table-->
                  <div id="Activities" class="card-panel">
                      <div class="">
                          <div class="nav-wrapper">
                            <form>
                              <!--Start of Search bar && Add Button-->
                              <div class="input-field">
                                <i class="mdi-action-search prefix input-field"></i>
                                <input id="icon_prefix" type="text">
                                <label for="icon_prefix">Search</label>
                                <a class="waves-effect waves-light btn modal-trigger" href="../views/add_activity.php" style="margin-left: 130px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add</a>
                              </div>
                              <!--End of Search bar && Add Button-->
                              <div class="divider"></div>
                            </form>
                          </div>

                          <table class="bordered highlight">
                            <thead>
                              <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Date Created</th>
                                <th>Created By</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Lorem</td>
                                <td>ipsum dolor</td>
                                <td>1/14/2018</td>
                                <td>devilarangel</td>
                                <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: grey;" href="#" data-activates='dropdown-menu-promotion'><i class="material-icons left">arrow_drop_down</i></a></td>
                                <!-- Dropdown Structure -->
                                <ul id='dropdown-menu-promotion' class='dropdown-content'>
                                  <li><a href="#viewPromotionModal" class="modal-trigger"><i class="material-icons">pageview</i>View</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#editActivityModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#deleteModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                                </ul>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                  </div>
                  <!--End of Activities Posts Table-->
                <!--End of contents-->
              </div>
              <!-- ================================================VIEW Modals================================================ -->
              <!--Start of View Announcement Modal-->
              <div id="viewAnnouncementModal" class="modal">
                <div class="modal-content">
                  <h4>Announcement</h4>
                  <h6><span style="font-style: bold;">Title:</span> Swimming Pool</h6>
                  <div class="divider"></div>
                  <p style="font-style: italic;">The village swimming pool will is under renovation. In line with this, it will be closed from
                    January 15 - January 30. Please do not hesistate to contact us for further concerns. Thank you for your consideration.</p>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
                </div>
              </div>
              <!--End of View Announcement Modal-->
              <!--Start of View Memorandum Modal-->
              <div id="viewMemorandumModal" class="modal">
                <div class="modal-content">
                  <h4>Memorandum</h4>
                  <h6><span style="font-style: bold;">Title:</span> Firecrackers</h6>
                  <div class="divider"></div>
                  <p style="font-style: italic;">In celebration of New Year's Eve, use of firecrackers are strictly prohibited inside the
                    village. The village roundabout will be the designated area for home owners fireworks display.</p>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
                </div>
              </div>
              <!--End of View Memorandum Modal-->
              <!--Start of View Reminder Modal-->
              <div id="viewReminderModal" class="modal">
                <div class="modal-content">
                  <h4>Reminder</h4>
                  <h6><span style="font-style: bold;">Title:</span> Lorem</h6>
                  <div class="divider"></div>
                  <p style="font-style: italic;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                     velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                     culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
                </div>
              </div>
              <!--End of View Reminder Modal-->
              <!--Start of View Activity Modal-->
              <div id="viewPromotionModal" class="modal">
                <div class="modal-content">
                  <h4>Promotion</h4>
                  <h6><span style="font-style: bold;">Title:</span> Lorem</h6>
                  <div class="divider"></div>
                  <p style="font-style: italic;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                     velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                     culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
                </div>
              </div>
              <!--End of View Activity Modal-->

              <!-- ================================================EDIT Modal================================================ -->
              <!--Start of Edit Announcement Modal-->
              <div id="editAnnouncementModal" class="modal">
                <div class="modal-content">
                  <h4>Announcement</h4>
                  <form class="col s12">
                    <h6><span style="font-weight: bold;">Title:</span></h6>
                    <div class="input-field inline">
                      <input id="streetName" type="text" class="validate">
                      <label for="streetName" data-error=""></label>
                    </div>
                    <div class="divider"></div>
                    <p style="font-weight: bold">Body:</p>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="body" class="materialize-textarea"></textarea>
                        <label for="body"></label>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn">Ok</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                </div>
              </div>
              <!--End of Edit Announcement Modal-->
              <!--Start of Edit Memo Modal-->
              <div id="editMemorandumModal" class="modal">
                <div class="modal-content">
                  <h4>Memorandum</h4>
                  <form class="col s12">
                    <h6><span style="font-weight: bold;">Title:</span></h6>
                    <div class="input-field inline">
                      <input id="streetName" type="text" class="validate">
                      <label for="streetName" data-error=""></label>
                    </div>
                    <div class="divider"></div>
                    <p style="font-weight: bold">Body:</p>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="body" class="materialize-textarea"></textarea>
                        <label for="body"></label>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn">Ok</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                </div>
              </div>
              <!--End of Edit Memo Modal-->
              <!--Start of Edit Reminder Modal-->
              <div id="editReminderModal" class="modal">
                <div class="modal-content">
                  <h4>Reminder</h4>
                  <form class="col s12">
                    <h6><span style="font-weight: bold;">Title:</span></h6>
                    <div class="input-field inline">
                      <input id="streetName" type="text" class="validate">
                      <label for="streetName" data-error=""></label>
                    </div>
                    <div class="divider"></div>
                    <p style="font-weight: bold">Body:</p>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="body" class="materialize-textarea"></textarea>
                        <label for="body"></label>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn">Ok</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                </div>
              </div>
              <!--End of Edit Reminder Modal-->
              <!--Start of Edit Activity Modal-->
              <div id="editActivityModal" class="modal">
                <div class="modal-content">
                  <h4>Activity</h4>
                  <form class="col s12">
                    <h6><span style="font-weight: bold;">Title:</span></h6>
                    <div class="input-field inline">
                      <input id="streetName" type="text" class="validate">
                      <label for="streetName" data-error=""></label>
                    </div>
                    <div class="divider"></div>
                    <p style="font-weight: bold">Body:</p>
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="body" class="materialize-textarea"></textarea>
                        <label for="body"></label>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn">Ok</a>
                  <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
                </div>
              </div>
              <!--End of Edit Activity Modal-->
              <!-- ================================================DELETE Modal================================================ -->
              <!-- Start of Delete Modal -->
              <div id="deleteModal" class="modal verify">
                <div class="modal-content row">
                  <a href="#!" style="float:right; color: grey;" class="modal-close"><i class="material-icons">close</i></a>
                  <form>
                    <p style="text-align: left">Please enter your password to confirm delete.</p>
                    <div class="input-field inline">
                      <input id="Password" type="password" class="validate">
                      <label for="Password"></label>
                    </div>
                  </form>
                  <a href="#!" style="float:right;" class="modal-action modal-close waves-effect waves-red btn red ">Delete</a>
                </div>
              </div>
              <!-- End of Delete Modal -->
          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });

          </script>
      </body>

      </html>
