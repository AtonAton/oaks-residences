<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="card-panel">
              <div class="row col s12 m12 l12">
                    <div class="card-panel col s12 m12 l12">
                      <!--Start of Search Bar && Add Admin Button-->
                      <div class="input-field">
                        <i class="mdi-action-search prefix input-field"></i>
                        <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                        <label for="icon_prefix">Search</label>
                        <a class="btn button-collapse" href="#" data-activates="slide-out" style="margin-left: 180px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add Home Owner</a>
                      </div>

                      <ul id="slide-out" class="side-nav">
                        <li><div class="user-view">
                          <div class="background">
                            <img src="images/office.jpg">
                          </div>
                          <a href="#!user"><img class="circle" src="images/yuna.jpg"></a>
                          <a href="#!name"><span class="white-text name">John Doe</span></a>
                          <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
                        </div></li>
                        <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
                        <li><a href="#!">Second Link</a></li>
                        <li><div class="divider"></div></li>
                        <li><a class="subheader">Subheader</a></li>
                        <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
                      </ul>

                      <!--End of Search bar && Add Admin Button-->
                      <div class="divider"></div>
                      <!--Start of Data Table for Admins-->
                        <table class="bordered highlight col s12 m12 l12 centered" id="myTable">
                          <thead>
                            <tr>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>House #</th>
                              <th>Street</th>
                              <th>Contact No.</th>
                              <th>Date Created</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Ma. Teresa</td>
                              <td>Loya</td>
                              <td>47</td>
                              <td>Dickens</td>
                              <td>09225637849</td>
                              <td>6/4/2004</td>
                              <td><a class="waves-effect waves-light btn modal-trigger" style="margin: 5px; background-color: #6E6702;" href="#modal1"><i class="material-icons left">create</i>Edit</a></td>
                            </tr>
                            <tr>
                              <td>Jane Michelle</td>
                              <td>Sarmiento</td>
                              <td>9</td>
                              <td>Mansfield</td>
                              <td>09164515761</td>
                              <td>4/12/2010</td>
                              <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">create</i>Edit</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
                  <!--End of Data Table for Admins-->


<!--=============================================================================================================================================-->


          </div>



          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });


            $('.button-collapse').sideNav({
                menuWidth: 300, // Default is 300
                edge: 'left', // Choose the horizontal origin
                closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true, // Choose whether you can drag to open on touch screens,
                onOpen: function(el) { /* Do Stuff* / }, // A function to be called when sideNav is opened*/
                onClose: function(el) { /* Do Stuff* / }, // A function to be called when sideNav is closed*/
              }
            );
          </script>

          <!--Script for search bar-->
          <script>
          function searchFilter() {
            // Declare variables
            var input, filter, table, tr, td, i, j;
            input = document.getElementById("icon_prefix search");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query

              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }
              }


          }
          </script>
      </body>

      </html>
