<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div style="">
            <?php include '../side_navigation.php';?>
          </div>
          <div id="main">


            <div class="hide-on-small-only">
                <div class="col s12 m12 l12" style="margin-left: 30px;">
                    <h5 class="breadcrumbs-title" style="color: #C05805;">Monthly Dues</h5>
                    <ol class="breadcrumbs">
                        <li><a href="index.php">Dashboard</a></li>
                        <li class="active">Monthly Dues</li>
                    </ol>
                </div>
            </div>
            <div class="card-panel" id="card-container">
              <div class="row">
                <div class="col s12">
                  <ul class="tabs tab-demo-active z-depth-1" style="width: 100%; background-color: #C05805;">
                    <li class="tab col s3" style="margin-top: 15px;"><a class="white-text waves-effect waves-light active" href="#currentDues">Current</a>
                    </li>
                    <li class="tab col s3" style="margin-top: 15px;"><a class="white-text waves-effect waves-light" href="#paymentHistory">Payment History</a>
                    </li>
                  <div class="indicator" style="right: 513px; left: 0px;"></div><div class="indicator" style="right: 513px; left: 0px;"></div></ul>
                </div>

                <div class="col s12">
                  <div id="currentDues" class="col s12" style="display: block;">
                    <table class="striped" id="bill">
                      <tbody>
                        <tr>
                          <td>Invoice #</td>
                          <td><?= $id_db_ho; ?></td>
                        </tr>
                        <tr>
                          <td>Bill Due Date</td>
                          <td><?= $date3; ?></td>
                        </tr>
                        <tr>
                          <td>Current Charges</td>
                          <td>₱ <?= $current_bill_ho; ?>.00</td>
                        </tr>
                        <tr>
                          <td>Previous Balance</td>
                          <td>₱ <?= $previous_bill_ho?>.00</td>
                        </tr>
                        <tr>
                          <td>Overdue Penalties</td>
                          <td>₱ <?= $penalty_ho; ?>.00</td>
                        </tr>
                        <tr>
                          <td>Credit</td>
                          <td>₱ <?= $credit_ho; ?>.00</td>
                        </tr>
                        <tr>
                          <td><strong>Total Charges</strong></td>
                          <td><strong>₱ <?= $totalBill_ho; ?>.00</strong></td>
                        </tr>
                      </tbody>
									  </table>
                  </div>
                  <!--Payment History-->
                  <div id="paymentHistory">
                    <ul class="collapsible collapsible-accordion" data-collapsible="accordion">
                      <li  class="">
                        <div class="collapsible-header " style="background-color: #9e9d8a; color: #FFF; width:100%; height: 40px; padding: 1px; font-style: italic; font-size: 20px; padding-bottom: 1px; " > <?= $date; ?>  </div>
                        <div class="collapsible-body col s12 m12 l12" style="">
                          <div class="row">
                            <div>
                              <table id="bill"class="striped">
                                <tbody>
                                  <tr>
                                    <td>Invoice #</td>
                                    <td><?= $id_db_ho; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Bill Due Date</td>
                                    <td><?= $date3; ?></td>
                                  </tr>
                                  <tr>
                                    <td>Current Charges</td>
                                    <td>₱ <?= $current_bill_ho; ?>.00</td>
                                  </tr>
                                  <tr>
                                    <td>Previous Balance</td>
                                    <td>₱ <?= $previous_bill_ho?>.00</td>
                                  </tr>
                                  <tr>
                                    <td>Overdue Penalties</td>
                                    <td>₱ <?= $penalty_ho; ?>.00</td>
                                  </tr>
                                  <tr>
                                    <td>Credit</td>
                                    <td>₱ <?= $credit_ho; ?>.00</td>
                                  </tr>
                                  <tr>
                                    <td><strong>Total Charges</strong></td>
                                    <td><strong>₱ <?= $totalBill_ho; ?>.00</strong></td>
                                  </tr>

                                  <tr>
                                    <td></td>
                                    <td></td>
                                  </tr>
                                  <tr>
                                    <td><strong>Ammount Paid</strong></td>
                                    <td><strong>₱ <?= $amount_paid_ho; ?>.00</strong></td>
                                  </tr>
                                  <tr>
                                    <td>Payment Date</td>
                                    <td><?= $payment_date_ho; ?></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>

                          </div><!--row-->
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>



              </div>
          </div>
        <!--Visit Modal End-->

          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
      </body>

      </html>
