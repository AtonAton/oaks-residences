<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <!--START OF GUARD MAIN DIV-->
          <div style="margin-top: 30px;">
            <div class="row">
              <div class="card-tabs">
                <ul class="tabs tabs-fixed-width">
                  <li class="tab"><a class="active" href="#HomeOwners">Home Owners</a></li>
                  <li class="tab"><a href="#VisitorsLog">Visitors </a></li>
                  <li class="tab"><a href="#Logs">Logs </a></li>
                </ul>
              </div>


              <div class="row col s12 m12 l12" id="HomeOwners">
                <div style="margin:30px;">
                  <div class="input-field" style="margin-bottom: 30px;">
                    <i class="mdi-action-search prefix input-field"></i>
                    <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                    <label for="icon_prefix">Search</label>
                  </div>
                  <!--End of Search bar && Add Admin Button-->
                  <div class="divider"></div>
                  <!--Start of Data Table for Admins-->
                  <table class="bordered2 highlight col s12 m12 l12 centered" id="myTable">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>House #</th>
                        <th>Street</th>
                        <th>Contact No.</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Irvin Jason</td>
                        <td>Layno</td>
                        <td>8</td>
                        <td>Milton</td>
                        <td>09055276708</td>
                        <td>
                          <button  class="waves-effect waves-light btn modal-trigger"  onclick="homeowner('<?=$account_id; ?>', '<?= $full_name; ?>' , '<?= $full_address; ?>')" style="margin: 5px; background-color: #6E6702;" href="#VisitModal"><i class="material-icons left"></i>Visit</buton>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <hr>
                  <ul class="pagination center" style="margin-top: 10px;">
                    <?php

                    if($page == 1 || $page == 0){

                      echo "<li class='disabled'><a href='#'><i class='mdi-navigation-chevron-left'></i></a></li>";

                    }else{

                      echo "<li class='waves-effect'><a href='index.php?page=".($page-1)."'><i class='mdi-navigation-chevron-left'></i></a></li>";


                    }


                    for($i = 1; $i <= $total_pages; $i++){

                      if($i == $page){

                        echo "<li class='waves-effect'><a class='active' href='index.php?page={$i}'>{$i}</a></li>";

                      }else{

                        echo "<li class='waves-effect'><a href='index.php?page={$i}'>{$i}</a></li>";

                      }



                    }



                    if($page == $total_pages){

                      echo"<li class='disabled'><a href='#'><i class='mdi-navigation-chevron-right'></i></a></li>";

                    }else{
                      echo"<li class='waves-effect'><a href='index.php?page=".($page+1)."'><i class='mdi-navigation-chevron-right'></i></a></li>";
                    }



                    ?>
                  </ul>
                </div>
              </div>

              <div class="row col s12 m12 l12" id="VisitorsLog">
                <div style="margin:30px;">
                  <!--Start of Search Bar && Add Admin Button-->
                  <div class="input-field" style="margin-bottom:30px;">
                    <i class="mdi-action-search prefix input-field"></i>
                    <input id="icon_prefix currentVisitors" type="text" onkeyup="searchCurrentVisitors()">
                    <label for="icon_prefix">Search</label>
                  </div>
                  <!--End of Search bar && Add Admin Button-->
                  <div class="divider"></div>
                  <!--Start of Data Table for Current Visitors-->
                  <table class="bordered2 highlight col s12 m12 l12 centered" id="currentVisitorsTable">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Home Owner</th>
                        <th>Purpose</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Darelle</td>
                        <td>Glovo</td>
                        <td>Ma. Teresa Loya</td>
                        <td>Visit</td>
                        <td>10:12</td>
                        <td></td>
                        <td>2/12/2018</td>
                        <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;">Out</a></td>
                      </tr>
                    </tbody>
                  </table>
                  <ul class="pagination center" style="margin-top: 10px;">
                    <?php

                    if($page == 1 || $page == 0){

                      echo "<li class='disabled'><a href='#'><i class='mdi-navigation-chevron-left'></i></a></li>";

                    }else{

                      echo "<li class='waves-effect'><a href='index.php?page=".($page-1)."'><i class='mdi-navigation-chevron-left'></i></a></li>";


                    }


                    for($i = 1; $i <= $total_pages; $i++){

                      if($i == $page){

                        echo "<li class='waves-effect'><a class='active' href='index.php?page={$i}'>{$i}</a></li>";

                      }else{

                        echo "<li class='waves-effect'><a href='index.php?page={$i}'>{$i}</a></li>";

                      }



                    }



                    if($page == $total_pages){

                      echo"<li class='disabled'><a href='#'><i class='mdi-navigation-chevron-right'></i></a></li>";

                    }else{
                      echo"<li class='waves-effect'><a href='index.php?page=".($page+1)."'><i class='mdi-navigation-chevron-right'></i></a></li>";
                    }



                    ?>
                  </ul>
                </div>
              </div>

              <div class="row col s12 m12 l12" id="Logs">
                <div style="margin: 30px;">
                  <!--Start of Search Bar && Add Admin Button-->
                  <div class="input-field" style="margin-bottom: 30px;">
                    <i class="mdi-action-search prefix input-field"></i>
                    <input id="icon_prefix visitorsLog" type="text" onkeyup="searchVisitorsLog()">
                    <label for="icon_prefix">Search</label>
                  </div>
                  <!--End of Search bar && Add Admin Button-->
                  <div class="divider"></div>
                  <!--Start of Data Table for Visitor log-->
                  <table class="bordered2 highlight col s12 m12 l12 centered" id="visitorsLogTable">
                    <thead>
                      <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Home Owner</th>
                        <th>Purpose</th>
                        <th>Time In</th>
                        <th>Time Out</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Jakeh</td>
                        <td>Rosh</td>
                        <td>Jane Michelle Sarmiento</td>
                        <td>Visit</td>
                        <td>8:27</td>
                        <td>9:32</td>
                        <td>2/8/2018</td>
                      </tr>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>

            </div>
          </div>
          <!--END OF GUARD MAIN DIV-->

          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>

  </body>
</html>
