<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="wrapper">
<!--Breadcrumb-->
                <div class="col s12 m12 l12" style="margin-left: 30px;">
                    <h5 class="breadcrumbs-title" style="color: #C05805;">Monthly Dues Individual Report</h5>
                    <ol class="breadcrumbs">
                        <li><a href="index.php">Dashboard</a></li>
                        <li><a href="reports_controller.php?source=reports">Reports</a></li>
                        <li class="active">Monthly Dues Individual Report</li>
                    </ol>
                </div>
<!--Breadcrumb-->
                <div class="card-panel">
                  <div class="row col s12 m12 l12">
                    <div class="card col s6 m6 l6">
                      <div class="input-field">
                        <i class="mdi-action-search prefix input-field"></i>
                        <input id="icon_prefix search" type="text" onkeyup="searchTable()">
                        <label for="icon_prefix">Search</label>
                      </div>
                      <!--End of Search bar-->
                      <div class="divider"></div>

                      <table class="highlight" id="myTable">
                        <thead>
                          <tr>
                              <th>First Name</th>
                              <th>Middle Name</th>
                              <th>Last Name</th>
                              <th></th>
                          </tr>
                        </thead>

                        <tbody>
                          <tr>
                            <td>Alvin</td>
                            <td>Eclair</td>
                            <td>Santos</td>
                            <td><a href="#" onclick="this.parentElement.parentElement.id = 'select'; select()"><i class="material-icons" style="color: #6E6702;">send</i></a></td>
                          </tr>
                          <tr>
                            <td>Alan</td>
                            <td>Jellybean</td>
                            <td>Dremar</td>
                            <td><a href="#" onclick="this.parentElement.parentElement.id = 'select'; select()"><i class="material-icons" style="color: #6E6702;">send</i></a></td>
                          </tr>
                          <tr>
                            <td>Jonathan</td>
                            <td>Lollipop</td>
                            <td>Saco</td>
                            <td><a href="#" onclick="this.parentElement.parentElement.id = 'select'; select()"><i class="material-icons" style="color: #6E6702;">send</i></a></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col s1 m1 l1">
                      &emsp;
                    </div>
                    <div class="card col s5 m5 l5">
                      <div class="row">
                        <div class="col s6 m6 l6">
                          <p>Payments Report for:</p>
                        </div>
                        <div class="col s6 m6 l6">
                          <input type="text" disabled id="selected_ho">
                        </div>
                        <div class="col s6 m6 l6">
                          <p>From:</p>
                        </div>
                        <div class="col s6 m6 l6">
                          <input id="dateFrom" type="text" class="datepicker">
                        </div>
                        <div class="col s6 m6 l6">
                          <p>To:</p>
                        </div>
                        <div class="col s6 m6 l6">
                          <input id="dateTo" type="text" class="datepicker">
                        </div>
                        <div class="col s12 m12 l12">
                          <button class="hidden modal-action modal-close waves-effect waves-green btn right" style="margin: 5px; background-color: #6E6702;" type="submit" name=""><i class="material-icons left">send</i>Generate</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>


          </div>


          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
          <script type="text/javascript">
            function select(){
              var fname, mname, lname, tr, txt;
              tr = document.getElementById('select').children;
              fname = tr[0].innerHTML;
              mname = tr[1].innerHTML;
              lname = tr[2].innerHTML;
              txt = fname+" "+mname+" "+lname;
              document.getElementById("selected_ho").value = txt;
            }
          </script>
          <script type="text/javascript">
          function searchTable() {
            var input, filter, found, table, tr, td, i, j;
            input = document.getElementById("icon_prefix search");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td");
                for (j = 0; j < td.length; j++) {
                    if (td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
                        found = true;
                    }
                }
                if (found) {
                    tr[i].style.display = "";
                    found = false;
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
          </script>
  </body>
</html>
