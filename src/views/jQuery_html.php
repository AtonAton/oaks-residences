              <div class="col s12 m12 l6">
                    <div class="card-panel">
                        <h4 class="header2">Form validation with placeholder</h4>
                        <p>Add class <code class="  language-markup">.left-alert</code> to show error message in left align.</p>
                        <!--add .left-alert to show messages left side-->
                        <div class="row">
                            <form class="col s12 left-alert">
                              <div class="row">
                                  <div class="input-field col s12">
                                    <input id="first_input1" class="validate" placeholder="John Doe" type="text">
                                    <label for="first_input1" data-error="Please enter first name." class="active">First Name</label>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="input-field col s12">
                                    <input id="age_input1" class="validate" placeholder="25" type="number">
                                    <label for="age_input1" data-error="Please enter your age." class="active">Age</label>
                                  </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <input id="email_input1" class="validate" placeholder="john@domainname.com" type="email">
                                  <label for="email_input1" class="active" data-error="Please enter valid email." data-success="I Like it!">Email</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <input id="url_input1" class="validate" placeholder="http://geekslabs.com/" type="url">
                                  <label for="url_input1" class="active" data-error="Please enter valid url.">URL</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <input id="password1" class="validate" placeholder="MonKey&amp;420@" type="password">
                                  <label for="password1" class="active">Password</label>
                                </div>
                              </div>
                              <div class="row">
                                <div class="input-field col s12">
                                  <textarea id="message1" class="materialize-textarea validate" length="120" placeholder="Oh WoW! Let me check this one too."></textarea>
                                  <label for="message1" class="active">Message</label>
                                <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span></div>
                             </div>
                            <div class="row">
                              <div class="input-field col s12">
                                <button class="btn waves-effect waves-light right" type="submit" name="action">Submit
                                  <i class="mdi-content-send right"></i>
                                </button>
                              </div>
                            </div>
                        </form>
                        </div>
                    </div>
                  </div>
