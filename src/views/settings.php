<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="card-panel">
                <!--Start of navigation-->
                <div class="card-tabs">
                  <ul class="tabs tabs-fixed-width">
                    <li class="tab"><a class="active" href="#Payments">Payments</a></li>
                    <li class="tab"><a href="#Visitors">Visitors</a></li>
                    <li class="tab"><a href="#Reminders">Settings 2</a></li>
                    <li class="tab"><a href="#Promotions">Settings 3</a></li>
                  </ul>
                </div>
                <!--End of navigation-->
                <!--Start of contents-->
                <div class="card-content grey lighten-4" id="Payments">
                  <!--Payments Start-->
                  <!--Monthly Dues Start-->
                  <div class="card-panel">
                      <div class="row">
                          <div class="nav-wrapper">
                            <h5 style="margin: 0px;">Monthly Dues Settings</h5>
                            <p class="desc">Change your monthly dues rates and payment schedule</p>
                            <div class="divider" style="margin-left: -20px; margin-right: -20px;"></div>
                          </div>
                          <form>
                            <table>
                              <tbody>
                                <tr><!--Start of rate-->
                                  <td><p style="font-weight: bold;">Rate:</p></td>
                                  <td>
                                    <div>
                                      <div class="input-field inline">
                                        <input id="rate" type="number" min="0" class="validate">
                                        <label for="rate" data-error="Value unacceptable. Negative value is invalid."></label>
                                      </div>
                                      <p class="desc">The rate for dues being charge to home owners every month.</p>
                                    </div>
                                  </td>
                                </tr><!--End of rate-->
                                <tr><!--Start of penalty-->
                                  <td><p style="font-weight: bold;">Penalty(%):</p></td>
                                  <td>
                                    <div>
                                      <div class="input-field inline">
                                        <input id="penalty" type="number" min="0" max="100"class="validate">
                                        <label for="penalty" data-error="Value unacceptable. Percentage 1% - 100%."></label>
                                      </div>
                                      <p class="desc">The percentage of penalty rate for overdues.</p>
                                    </div>
                                  </td>
                                </tr><!--End of penalty-->
                                <tr><!--Start of penalty date-->
                                  <td><p style="font-weight: bold;">Day of Penalty:</p></td>
                                  <td>
                                    <div>
                                      <div class="input-field inline">
                                        <input id="penaltydate" type="number" min="1" max="30" class="validate">
                                        <label for="penaltydate" data-error="Value unacceptable. Choose a day of the month (1-30)."></label>
                                      </div>
                                      <p class="desc">The day for which penalty for overdues will apply.</p>
                                    </div>
                                  </td>
                                </tr><!--End of penalty date-->
                                <tr><!--Start of release invoice date-->
                                  <td><p style="font-weight: bold;">Day of Invoice Release:</p></td>
                                  <td>
                                    <div>
                                      <div class="input-field inline">
                                        <input id="releasedate" type="number" min="1" max="30" class="validate">
                                        <label for="releasedate" data-error="Value unacceptable. Choose a day of the month (1-30)."></label>
                                      </div>
                                      <p class="desc">The day for which the next bill is generated.</p>
                                    </div>
                                  </td>
                                </tr><!--End of release invoice date-->
                              </tbody>
                            </table>
                          </form>
                      </div>
                  </div>
                  <div class="card-panel">
                      <div class="row">
                          <div class="nav-wrapper">
                            <h5 style="margin: 0px;">Parking Fee Settings</h5>
                            <p class="desc">Change yourparking fee rates and payment schedule</p>
                            <div class="divider" style="margin-left: -20px; margin-right: -20px;"></div>
                          </div>
                          <form>
                            <table>
                              <tbody>
                                <tr><!--Start of rate-->
                                  <td><p style="font-weight: bold;">Rate:</p></td>
                                  <td>
                                    <div>
                                      <div class="input-field inline">
                                        <input id="rate" type="number" min="0" class="validate">
                                        <label for="rate" data-error="Value unacceptable. Negative value is invalid."></label>
                                      </div>
                                      <p class="desc">The rate for monthly parking fee.</p>
                                    </div>
                                  </td>
                                </tr><!--End of rate-->
                                <tr><!--Start of release invoice date-->
                                  <td><p style="font-weight: bold;">Day of Invoice Release:</p></td>
                                  <td>
                                    <div>
                                      <div class="input-field inline">
                                        <input id="releasedate" type="number" min="1" max="30" class="validate">
                                        <label for="releasedate" data-error="Value unacceptable. Choose a day of the month (1-30)."></label>
                                      </div>
                                      <p class="desc">The day for which the next bill is generated.</p>
                                    </div>
                                  </td>
                                </tr><!--End of release invoice date-->
                              </tbody>
                            </table>
                          </form>
                      </div>
                  </div>
                </div>
                  <!--Payments End-->
                  <!--Visitors Start-->
                <div class="card-panel" id="Visitors">
                  <div>
                    <div class="input-field">
                      <i class="mdi-action-search prefix input-field"></i>
                      <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                      <label for="icon_prefix">Search</label>
                    </div>
                    <!--End of Search bar && Add Admin Button-->
                    <div class="divider"></div>
                    <table class="bordered highlight col s12 m12 l12 centered" id="myTable">
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>House #</th>
                          <th>Street</th>
                          <th>Contact No.</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Ma. Teresa</td>
                          <td>Loya</td>
                          <td>47</td>
                          <td>Dickens</td>
                          <td>09225637849</td>
                          <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;" onclick="showEditVisitorsDiv()"><i class="material-icons left">create</i>Edit</a></td>
                        </tr>
                        <tr>
                          <td>Jane Michelle</td>
                          <td>Sarmiento</td>
                          <td>9</td>
                          <td>Mansfield</td>
                          <td>09164515761</td>
                          <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">create</i>Edit</a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div id="visitorDiv" style="display: none;">
                    <div class="card-panel row">
                      <div>
                        <h5 class="center" style="font-style: italic; color: #6E6702;">LIST OF BANS</h5>
                      </div>
                      <div class="divider"></div>
                      <div>
                        <div class="card-tabs">
                          <ul class="tabs tabs-fixed-width">
                            <li class="tab"><a class="active" href="#Individuals">Individuals</a></li>
                            <li class="tab"><a href="#Vehicles">Vehicles</a></li>
                          </ul>
                        </div>
                        <!--Start of Content-->
                        <!--Start of Individuals Tab-->
                        <script>
                        function searchIndividuals() {
                          // Declare variables
                          var input, filter, table, tr, td, i, j;
                          input = document.getElementById("icon_prefix searchIndividuals");
                          filter = input.value.toUpperCase();
                          table = document.getElementById("banIndividualsTable");
                          tr = table.getElementsByTagName("tr");

                          // Loop through all table rows, and hide those who don't match the search query

                            for (i = 0; i < tr.length; i++) {
                              td = tr[i].getElementsByTagName("td")[0];
                              if (td) {
                                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                  tr[i].style.display = "";
                                } else {
                                  tr[i].style.display = "none";
                                }
                              }
                            }
                        }
                        </script>
                        <div class="card-content" id="Individuals">
                          <div class="row">
                            <div class="input-field">
                              <i class="mdi-action-search prefix input-field"></i>
                              <input id="icon_prefix searchIndividuals" type="text" onkeyup="searchIndividuals()">
                              <label for="icon_prefix searchIndividuals">Search</label>
                              <a class="waves-effect waves-light btn modal-trigger" href="#addIndividualBanModal" style="margin-left: 180px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add</a>
                            </div>
                            <div>
                              <table class="bordered highlight col s12 m12 l12 centered" id="banIndividualsTable">
                                <thead>
                                  <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Gender</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Rogelio</td>
                                    <td>Romualdo</td>
                                    <td>Male</td>
                                    <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: Grey;" href="#" data-activates='dropdown-menu-announcement'><i class="material-icons left">arrow_drop_down</i></a></td>
                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-menu-announcement' class='dropdown-content'>
                                      <li><a href="#editIndividualBanModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#deleteIndividualBanModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                                    </ul>
                                  </tr>
                                  <tr>
                                    <td>Adrielle</td>
                                    <td>Rivera</td>
                                    <td>Male</td>
                                    <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: Grey;" href="#" data-activates='dropdown-menu-announcement'><i class="material-icons left">arrow_drop_down</i></a></td>
                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-menu-announcement' class='dropdown-content'>
                                      <li><a href="#editIndividualBanModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#deleteIndividualBanModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                                    </ul>
                                  </tr>
                                  <tr>
                                    <td>Jona</td>
                                    <td>Detaunan</td>
                                    <td>Female</td>
                                    <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: Grey;" href="#" data-activates='dropdown-menu-announcement'><i class="material-icons left">arrow_drop_down</i></a></td>
                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-menu-announcement' class='dropdown-content'>
                                      <li><a href="#editIndividualBanModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#deleteIndividualBanModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                                    </ul>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <!--End of Individuals Tab-->
                        <!--Start of Vehicles Tab-->
                        <script>
                        function searchVehicles() {
                          // Declare variables
                          var input, filter, table, tr, td, i, j;
                          input = document.getElementById("icon_prefix searchVehicles");
                          filter = input.value.toUpperCase();
                          table = document.getElementById("banVehiclesTable");
                          tr = table.getElementsByTagName("tr");

                          // Loop through all table rows, and hide those who don't match the search query

                            for (i = 0; i < tr.length; i++) {
                              td = tr[i].getElementsByTagName("td")[3];
                              if (td) {
                                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                                  tr[i].style.display = "";
                                } else {
                                  tr[i].style.display = "none";
                                }
                              }
                            }
                        }
                        </script>
                        <div class="card-content" id="Vehicles">
                          <div class="row">
                            <div class="input-field">
                              <i class="mdi-action-search prefix input-field"></i>
                              <input id="icon_prefix searchVehicles" type="text" onkeyup="searchVehicles()">
                              <label for="icon_prefix searchVehicles">Search Plate No.</label>
                              <a class="waves-effect waves-light btn modal-trigger" href="#addVehicleBanModal" style="margin-left: 180px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add</a>
                            </div>
                            <div>
                              <table class="bordered highlight col s12 m12 l12 centered" id="banVehiclesTable">
                                <thead>
                                  <tr>
                                    <th>Model</th>
                                    <th>Year</th>
                                    <th>Color</th>
                                    <th>Plate No.</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>Nissan 370z</td>
                                    <td>2009</td>
                                    <td>Black</td>
                                    <td>VY4276</td>
                                    <td><a class="waves-effect waves-light dropdown-button" style="margin: 5px; color: Grey;" href="#" data-activates='dropdown-menu-vehicle'><i class="material-icons left">arrow_drop_down</i></a></td>
                                    <!-- Dropdown Structure -->
                                    <ul id='dropdown-menu-vehicle' class='dropdown-content'>
                                      <li><a href="#editVehicleBanModal" class="modal-trigger"><i class="material-icons">create</i>Edit</a></li>
                                      <li class="divider"></li>
                                      <li><a href="#deleteVehicleBanModal" class="modal-trigger"><i class="material-icons">close</i>Delete</a></li>
                                    </ul>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <!--End of Vehicles Tab-->
                        <!-- =====INDIVIDUALS MODALS===== -->
                        <!--Add Ban Individual Modal-->
                        <div id="addIndividualBanModal" class="modal">
                          <div class="modal-content">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" style="float: right;"><i class="material-icons">clear</i></a>
                            <h5 class="center">Add Banned Individual</h5>
                            <div class="row">
                              <form>
                                <div class="input-field col s6 m6 l6">
                                  <input id="FirstName" type="text" class="validate">
                                  <label for="FirstName">First Name</label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input id="LastName" type="text" class="validate">
                                  <label for="LastName">Last Name</label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <select>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                  </select>
                                  <label>Gender</label>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Accept</a>
                          </div>
                        </div>
                        <!--Edit Ban Individual Modal-->
                        <div id="editIndividualBanModal" class="modal">
                          <div class="modal-content">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" style="float: right;"><i class="material-icons">clear</i></a>
                            <h5 class="center">Edit</h5>
                            <div class="row">
                              <form>
                                <div class="input-field col s6 m6 l6">
                                  <input placeholder="Rogelio" id="FirstName" type="text" class="validate">
                                  <label for="FirstName"></label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input placeholder="Romualdo" id="LastName" type="text" class="validate">
                                  <label for="LastName"></label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <select>
                                    <option value="1">Male</option>
                                    <option value="2">Female</option>
                                  </select>
                                  <label>Gender</label>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save</a>
                          </div>
                        </div>
                        <!--Delete Ban Individual Modal-->
                        <div id="deleteIndividualBanModal" class="modal verify">
                          <div class="modal-content row">
                            <a href="#!" style="float:right; color: grey;" class="modal-close"><i class="material-icons">close</i></a>
                            <form>
                              <p style="text-align: left">Please enter your password to confirm delete.</p>
                              <div class="input-field inline">
                                <input id="Password" type="password" class="validate">
                                <label for="Password"></label>
                              </div>
                            </form>
                            <a href="#!" style="float:right;" class="modal-action modal-close waves-effect waves-red btn red ">Delete</a>
                          </div>
                        </div>

                        <!--=========BAN VEHICLES MODALS=========-->

                        <!--Add Ban Vehicle Modal-->
                        <div id="addVehicleBanModal" class="modal">
                          <div class="modal-content">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" style="float: right;"><i class="material-icons">clear</i></a>
                            <h5 class="center">Add Banned Vehicle</h5>
                            <div class="row">
                              <form>
                                <div class="input-field col s6 m6 l6">
                                  <input id="Model" type="text" class="validate">
                                  <label for="Model">Model</label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input id="Year" type="text" class="validate">
                                  <label for="Year">Year</label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input id="Color" type="text" class="validate">
                                  <label for="Color">Color</label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input id="PlateNum" type="text" class="validate">
                                  <label for="PlateNum">Plate No.</label>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Accept</a>
                          </div>
                        </div>
                        <!--Edit Ban Vehicle Modal-->
                        <div id="editVehicleBanModal" class="modal">
                          <div class="modal-content">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" style="float: right;"><i class="material-icons">clear</i></a>
                            <h5 class="center">Edit</h5>
                            <div class="row">
                              <form>
                                <div class="input-field col s6 m6 l6">
                                  <input placeholder="Nissan 370z" id="Model" type="text" class="validate">
                                  <label for="Model"></label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input placeholder="2009" id="Year" type="text" class="validate">
                                  <label for="Year"></label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input placeholder="Black" placeholder="" id="Color" type="text" class="validate">
                                  <label for="Color"></label>
                                </div>
                                <div class="input-field col s6 m6 l6">
                                  <input placeholder="VY4276" id="PlateNum" type="text" class="validate">
                                  <label for="PlateNum"></label>
                                </div>
                              </form>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Save</a>
                          </div>
                        </div>
                        <!--Delete Ban Vehicle Modal-->
                        <div id="deleteVehicleBanModal" class="modal verify">
                          <div class="modal-content row">
                            <a href="#!" style="float:right; color: grey;" class="modal-close"><i class="material-icons">close</i></a>
                            <form>
                              <p style="text-align: left">Please enter your password to confirm delete.</p>
                              <div class="input-field inline">
                                <input id="Password" type="password" class="validate">
                                <label for="Password"></label>
                              </div>
                            </form>
                            <a href="#!" style="float:right;" class="modal-action modal-close waves-effect waves-red btn red ">Delete</a>
                          </div>


                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--Visitors End-->

              </div>




          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });

          </script>
          <!--Script for search bar-->
          <script>
          function searchFilter() {
            // Declare variables
            var input, filter, table, tr, td, i, j;
            input = document.getElementById("icon_prefix search");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query

              for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                  if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                  } else {
                    tr[i].style.display = "none";
                  }
                }
              }
          }
          </script>

          <!--Script for edit visitors div-->
          <script type="text/javascript">
          function showEditVisitorsDiv(){
            var editDiv;
            editDiv = document.getElementById("visitorDiv");
            if(editDiv.style.display == ""){
              editDiv.style.display = "none";
            }
            else{
              editDiv.style.display = "";
            }
          }
          </script>
      </body>

      </html>
