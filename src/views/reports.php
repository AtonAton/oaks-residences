<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Oaks Residences</title>

    <!--Materialize Icons-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Favicons-->
    <link rel="icon" href="../resources/images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="../resources/images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


</head>

<body>
    <header id="header" class="page-topbar">
      <?php include '../ho_header_topbar.php';?>
    </header>
    <div id="main">
      <!-- <div class="card-panel"> -->
      <div class="main s12 m7 l4 post_page">
        <div style="padding:10px;">
          <div class="row">
            <div class="card col s4 m4 l4" style="padding: 0px; border-radius: 25px;">
              <div class="col s3 m3 l3" style="background-color: #DB9501; color: #FFF; height: 60px;">
                <i class="medium material-icons">settings</i>
              </div>
              <div class="col s9 m9 l9">
                dito yung links
              </div>
            </div>
          </div>
        </div>
              <div class="col s4 m4 l4">
                <ul class="collection">
                  <li class="collection-item avatar">
                      <i class="mdi-action-assessment circle green"></i>
                    <span class="title">Monthly Dues Master List Report</span>
                    <p class="desc">alskdaskdh
                    </p>
                    <a target="_blank" href="includes/report_h_o_list_page.php" class="secondary-content"><i class="material-icons">send</i></a>
                  </li>
                </ul>
              </div>
    </div>
  <!-- </div> -->
    <!-- ================================================Scripts================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="../resources/js/custom-script.js"></script>

    <!--Script for modals-->
    <script type="text/javascript">

      $(document).ready(function(){
        $('.modal').modal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            inDuration: 300, // Transition in duration
            outDuration: 200, // Transition out duration
            startingTop: '1%', // Starting top style attribute
            endingTop: '10%', // Ending top style attribute
            ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
              alert("Ready");
              console.log(modal, trigger);
            },
            complete: function() { alert('Closed'); } // Callback for Modal close
          }
        );

      });

    </script>
</body>

</html>
