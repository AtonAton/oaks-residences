<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <!-- <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,"> -->
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">


      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div>
            <?php include '../ho_side_navigation.php';?>
            <form class="col s12">
              <div class="row">
                <!--Start of Personal Details-->
                <div style="background-color: #6E6702; color: #FFF; width:100%; height: 40px; padding: 1px;">
                  <h4 style="font-style: italic; font-size: 20px; padding-bottom: 1px;">Personal Details</h4>
                </div>
                <div class="row">
                  <ul id="form-inline">
                    <li><p style="color:#6E6702; margin-left: 5px;">First Name:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="first_name" type="text" class="validate">
                        <label for="first_name">Maria</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Middle Name:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="middle_name" type="text" class="validate">
                        <label for="middle_name">Isolde</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Last Name:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="last_name" type="text" class="validate">
                        <label for="last_name">Villanueva</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Gender:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="gender" type="text" class="validate">
                        <label for="gender">Female</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Birthday:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="gender" type="text" class="validate">
                        <label for="gender">06/22/1982</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">House #:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="gender" type="text" class="validate">
                        <label for="gender">7</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Street:</p></li>
                    <li>
                      <div class="input-field">
                        <input disabled placeholder="" id="gender" type="text" class="validate">
                        <label for="gender">Dickens</label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Contact No:</p></li>
                    <li>
                      <div class="input-field">
                        <input placeholder="(+63)995 432 6187" id="contact#" type="text" class="validate">
                        <label for="contact#"></label>
                      </div>
                    </li>
                    <li><p style="color:#6E6702; margin-left: 5px;">Telephone No:</p></li>
                    <li>
                      <div class="input-field">
                        <input placeholder="(02)421 5564" id="tel" type="tel" class="validate">
                        <label for="tel"></label>
                      </div>
                    </li>
                  </ul>
                  <!--End of Personal Details-->
                  <!--Start of Account Details-->
                  <div style="background-color: #6E6702; color: #FFF; width:100%; height: 40px; padding: 1px;">
                    <h4 style="font-style: italic; font-size: 20px; padding-bottom: 1px;">Account Details</h4>
                  </div>
                  <div class="row">
                    <ul id="form-inline">
                      <li><p style="color:#6E6702; margin-left: 5px;">Email:</p></li>
                      <li>
                        <div class="input-field">
                          <input placeholder="fruit.mango@gmail.com" id="email" type="tel" class="validate">
                          <label for="email"></label>
                        </div>
                      </li>
                      <li><p style="color:#6E6702; margin-left: 5px;">Username:</p></li>
                      <li>
                        <div class="input-field">
                          <input disabled placeholder="" id="username" type="tel" class="validate">
                          <label for="username">fruit.mango</label>
                        </div>
                      </li>
                      <li><p style="color:#6E6702; margin-left: 5px;">Password:</p></li>
                      <li>
                        <div class="input-field">
                          <input placeholder="**********" id="pass" type="password" class="validate">
                          <label for="pass"></label>
                        </div>
                      </li>
                    </ul>
                    <!--End of Account Details-->
                    <!--Start of Alternate Contact Details-->
                    <div style="background-color: #6E6702; color: #FFF; width:100%; height: 40px; padding: 1px;">
                      <h4 style="font-style: italic; font-size: 20px; padding-bottom: 1px;">Alternate Contact Details</h4>
                    </div>
                    <div class="row">
                      <ul id="form-inline">
                        <li><p style="color:#6E6702; margin-left: 5px;">First Name:</p></li>
                        <li>
                          <div class="input-field">
                            <input placeholder="Hazel" id="first_name" type="text" class="validate">
                            <label for="first_name"></label>
                          </div>
                        </li>
                        <li><p style="color:#6E6702; margin-left: 5px;">Middle Name:</p></li>
                        <li>
                          <div class="input-field">
                            <input placeholder="Pogi" id="middle_name" type="text" class="validate">
                            <label for="middle_name"></label>
                          </div>
                        </li>
                        <li><p style="color:#6E6702; margin-left: 5px;">Last Name:</p></li>
                        <li>
                          <div class="input-field">
                            <input placeholder="Que" id="last_name" type="text" class="validate">
                            <label for="last_name"></label>
                          </div>
                        </li>
                        <li><p style="color:#6E6702; margin-left: 5px;">Contact No:</p></li>
                        <li>
                          <div class="input-field">
                            <input placeholder="(+63)927 556 3214" id="contact#" type="text" class="validate">
                            <label for="contact#"></label>
                          </div>
                        </li>
                      </ul>
                      <!--End of Alternate Contact Details-->
                </div>
                <div class="footer" style="padding: 10px; float: right;">
                  <a class="btn waves-effect waves-light grey" href="#"><i class="material-icons left">clear</i>Cancel</a>
                  <a class="btn waves-effect waves-light orange" href="#"><i class="material-icons left">save</i>Save</a>
                </div>
              </div>
            </form>
          </div>

          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script>
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });
          </script>
      </body>

      </html>
      <!-- <div class="card-panel" style="background-color:#FFF;">
        <div class="row">
          <form class="col s12">
            <div class="row">
              <div class="row" style="width: 100%; margin:0px;">
                <div style="background-color:#C05805;">
                  <p>Personal Details</p>
                </div>
                <div class="input-field col s6">
                  <input disabled placeholder="" id="first_name" type="text" class="validate">
                  <label for="first_name">Maria</label>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <input disabled placeholder="" id="middle_name" type="text" class="validate">
                    <label for="middle_name">Isolde</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s6">
                    <input disabled placeholder="" id="last_name" type="text" class="validate">
                    <label for="last_name">Villanueva</label>
                  </div>
                </div>
              </div>

            <div class="row">
              <div class="input-field col s12">
                <input id="password" type="password" class="validate">
                <label for="password">Password</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <input id="email" type="email" class="validate">
                <label for="email">Email</label>
              </div>
            </div>
            <div class="row">
              <div class="col s12">
                This is an inline input field:
                <div class="input-field inline">
                  <input id="email" type="email" class="validate">
                  <label for="email" data-error="wrong" data-success="right">Email</label>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div> -->
