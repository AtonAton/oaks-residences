<!DOCTYPE html>
<html lang="en">

      <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="msapplication-tap-highlight" content="no">
          <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
          <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
          <title>Oaks Residences</title>

          <!--Materialize Icons-->
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
          <!-- Favicons-->
          <link rel="icon" href="resources/images/favicon/favicon-32x32.png" sizes="32x32">
          <!-- Favicons-->
          <link rel="apple-touch-icon-precomposed" href="resources/images/favicon/apple-touch-icon-152x152.png">
          <!-- For iPhone -->
          <meta name="msapplication-TileColor" content="#00bcd4">
          <meta name="msapplication-TileImage" content="../resources/images/favicon/mstile-144x144.png">
          <!-- For Windows Phone -->


          <!-- CORE CSS-->
          <link href="../resources/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
          <!-- Custome CSS-->
          <link href="../resources/css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


          <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
          <link href="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
          <link href="../resources/js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">



      </head>

      <body>
          <header id="header" class="page-topbar">
            <?php include '../header_topbar.php';?>
          </header>
          <div id="main">
              <?php include '../side_navigation.php';?>
              <div class="card-panel">
              <div class="row col s12 m12 l12">
                    <div class="card-panel col s12 m12 l12">
                      <!--Start of Search Bar && Add Admin Button-->
                      <div class="input-field">
                        <i class="mdi-action-search prefix input-field"></i>
                        <input id="icon_prefix search" type="text" onkeyup="searchFilter()">
                        <label for="icon_prefix">Search</label>
                        <a class="waves-effect waves-light btn modal-trigger" href="../views/add_homeOwner.php" style="margin-left: 180px; margin-bottom: 20px; margin-top: 20px; background-color: #C05805;"><i class="material-icons left">add</i>Add Home Owner</a>
                      </div>
                      <!--End of Search bar && Add Admin Button-->
                      <div class="divider"></div>
                      <!--Start of Data Table for Admins-->
                        <table class="bordered highlight col s12 m12 l12 centered" id="myTable">
                          <thead>
                            <tr>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>House #</th>
                              <th>Street</th>
                              <th>Contact No.</th>
                              <th>Date Created</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Ma. Teresa</td>
                              <td>Loya</td>
                              <td>47</td>
                              <td>Dickens</td>
                              <td>09225637849</td>
                              <td>6/4/2004</td>
                              <td><a class="waves-effect waves-light btn modal-trigger" style="margin: 5px; background-color: #6E6702;" href="#modal1"><i class="material-icons left">create</i>Edit</a></td>
                            </tr>
                            <tr>
                              <td>Jane Michelle</td>
                              <td>Sarmiento</td>
                              <td>9</td>
                              <td>Mansfield</td>
                              <td>09164515761</td>
                              <td>4/12/2010</td>
                              <td><a class="waves-effect waves-light btn" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">create</i>Edit</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
                  <!--End of Data Table for Admins-->

                  <!--Start of Edit Modal-->
                  <div id="modal1" class="modal modal-fixed-footer">
                    <div class="modal-content">
                      <h4></h4>
                      <div class="row">
                        <form class="s12">
                          <div class="row"><!--End of Account Details Div-->
                            <h5 style="margin: 20px;color: #C05805;">Account Details</h5> <!--Account Details Fields-->
                            <div class="divider"></div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Username" id="username" type="text" class="validate">
                                <label for="username">Username</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Password" id="password" type="password" class="validate">
                                <label for="password">Password</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="" id="confirmPassword" type="password" class="validate">
                                <label for="confirmPassword">Confirm Password</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Email" id="email" type="password" class="validate">
                                <label for="email">Email</label>
                              </div>
                            </div><!--End of Account Details Div-->
                            <div class="row"><!--Start of Personal Details Div-->
                              <div class="divider"></div>
                              <h5 style="margin: 20px;color: #C05805;">Personal Details</h5> <!--Personal Details Fields-->
                              <div class="divider"></div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="First Name" id="first_name" type="text" class="validate">
                                <label for="first_name">First Name</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Middle Name" id="middle_name" type="text" class="validate">
                                <label for="middle_name">Middle Name</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Last Name" id="last_name" type="text" class="validate">
                                <label for="last_name">Last Name</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Phone" id="phone" type="text" class="validate">
                                <label for="phone">Phone</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Mobile" id="mobile" type="text" class="validate">
                                <label for="mobile">Mobile</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="House Number" id="houseNum" type="text" class="validate">
                                <label for="houseNum">House Number</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Street" id="street" type="text" class="validate">
                                <label for="street">Street</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <p>
                                  <p style="font-size: 15px">Gender</p>
                                  <input name="group1" type="radio" id="test1" />
                                  <label for="test1">Male</label>
                                  <input name="group1" type="radio" id="test2" />
                                  <label for="test2">Female</label>
                                </p>
                              </div>
                              <div class="col s12 m6 l6">
                                <p style="font-size: 15px">Birth Date</p>
                                <input type="text" class="datepicker" id="birthdate">
                              </div>
                            </div><!--End of Personal Details Div-->
                            <div class="row"><!--Start of Alternate Contact Div-->
                              <div class="divider"></div>
                              <h5 style="margin: 20px; color: #C05805; ">Alternate Contact</h5> <!--Personal Details Fields-->
                              <div class="divider"></div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="First Name" id="first_name" type="text" class="validate">
                                <label for="first_name">First Name</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Middle Name" id="middle_name" type="text" class="validate">
                                <label for="middle_name">Middle Name</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Last Name" id="last_name" type="text" class="validate">
                                <label for="last_name">Last Name</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Phone" id="phone" type="text" class="validate">
                                <label for="phone">Phone</label>
                              </div>
                              <div class="input-field col s12 m6 l6">
                                <input placeholder="Mobile" id="mobile" type="text" class="validate">
                                <label for="mobile">Mobile</label>
                              </div>
                            </div><!--End of Alternate Contact Div-->
                          </div>
                        </form>
                      <div class="footer">
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #6E6702;"><i class="material-icons left">save</i>Save</a>
                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn" style="margin: 5px; background-color: #444;"><i class="material-icons left">clear</i>Cancel</a>
                      </div>
                    </div>
                    </div>
                  </div>
                  <!--End of Edit Modal-->

<!--=============================================================================================================================================-->


          </div>



          <!-- ================================================Scripts================================================ -->

          <!-- jQuery Library -->
          <script type="text/javascript" src="../resources/js/plugins/jquery-1.11.2.min.js"></script>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
          <!--materialize js-->
          <script type="text/javascript" src="../resources/js/materialize.min.js"></script>
          <!--scrollbar-->
          <script type="text/javascript" src="../resources/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

          <!--plugins.js - Some Specific JS codes for Plugin Settings-->
          <script type="text/javascript" src="../resources/js/plugins.min.js"></script>
          <!--custom-script.js - Add your own theme custom JS-->
          <script type="text/javascript" src="../resources/js/custom-script.js"></script>

          <!--Script for modals-->
          <!-- <script type="text/javascript">

            $(document).ready(function(){
              $('.modal').modal({
                  dismissible: true, // Modal can be dismissed by clicking outside of the modal
                  opacity: .5, // Opacity of modal background
                  inDuration: 300, // Transition in duration
                  outDuration: 200, // Transition out duration
                  startingTop: '1%', // Starting top style attribute
                  endingTop: '10%', // Ending top style attribute
                  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    alert("Ready");
                    console.log(modal, trigger);
                  },
                  complete: function() { alert('Closed'); } // Callback for Modal close
                }
              );

            });
          </script> -->
          <!--Script for datepicker-->
          <script type="text/javascript">
            $('.datepicker').pickadate({
              selectMonths: true, // Creates a dropdown to control month
              selectYears: 15, // Creates a dropdown of 15 years to control year,
              today: 'Today',
              clear: 'Clear',
              close: 'Ok',
              closeOnSelect: false // Close upon selecting a date,
            });


            $('.button-collapse').sideNav({
                menuWidth: 300,
                edge: 'left',
                closeOnClick: true,
                draggable: true,
                onOpen: function(el){},
                onClose: function(el) {}
              }
            );
          </script>

          <!--Script for search bar-->
          // <script>
          // function searchFilter() {
          //   var $rows = $('#myTable tr');
          //   $('#icon_prefix search').keyup(function() {
          //
          //       var val = '^(?=.*\\b' + $.trim($(this).val()).split(/\s+/).join('\\b)(?=.*\\b') + ').*$',
          //           reg = RegExp(val, 'i'),
          //           text;
          //
          //       $rows.show().filter(function() {
          //           text = $(this).text().replace(/\s+/g, ' ');
          //           return !reg.test(text);
          //       }).hide();
          //   });
          //
          //
          //
          //
          //
          // }
          // </script>
          <script>
          $(document).ready(function(){
            $("#icon_prefix search").on("keyup", function() {
              var value = $(this).val().toLowerCase();
              $("#myTable tr").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
              });
            });
          });
          </script>
      </body>

      </html>
