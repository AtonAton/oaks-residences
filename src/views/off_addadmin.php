<div class="col s12 m12 l12" style="margin-left: 30px;">
    <h5 class="breadcrumbs-title" style="color: #C05805;">Payment Settings</h5>
    <ol class="breadcrumbs">
        <li><a href="index.php">Dashboard</a></li>
        <li><a href="#">Payments</a></li>
        <li><a href="postsMain.php?source=manage_posts">Manage Posts</a></li>
        <li class="active">Payment Settings</li>
    </ol>
</div>

<div class="col s12 m12 l12" style="margin-left: 30px;">
    <h5 class="breadcrumbs-title" style="color: #C05805;">Reports</h5>
    <ol class="breadcrumbs">
        <li><a href="index.php">Dashboard</a></li>
        <li class="active">Reports</li>
    </ol>
</div>







 </div>
 <!-- END MAIN -->

 <!-- START FOOTER -->


     <!-- END FOOTER -->

<!-- ================================================Scripts================================================ -->

 <!-- jQuery Library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
 <!-- <script type="text/javascript" src="../js/plugins/jquery-1.11.2.min.js"></script> -->
 <script type="text/javascript" src="../js/jQuery_validation.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
 <!--materialize js-->
 <script type="text/javascript" src="../js/materialize.min.js"></script>
 <!--scrollbar-->
 <script type="text/javascript" src="../js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
 <script type="text/javascript" src="../js/plugins/jquery.timeago.js" ></script>
 <!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script> -->
 <!--plugins.js - Some Specific JS codes for Plugin Settings-->
 <script type="text/javascript" src="../js/plugins.min.js"></script>
 <!--custom-script.js - Add your own theme custom JS-->
 <script type="text/javascript" src="../js/custom-script.js"></script>
 <script type="text/javascript" src="../js/jquery.easing.1.3.js.js"></script>
 <!--<script type="text/javascript" src="../js/tcal.js"></script> -->
 <!--<script type="text/javascript" src="../js/select2.min.js"></script>-->
 <!-- <script type="text/javascript" src="../assets/swal2/sweetalert2.min.js"></script>   -->
 <!--<script type="text/javascript" src="../js/datatables.js"></script>-->
 <script type="text/javascript" src="../js/plugins/sweetalert/sweetalert.min.js" ></script>
 <script src="../js/application.js" type="text/javascript" charset="utf-8"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>




<script type="text/javascript">
var tableToList = new List('test-list', {
valueNames: ['name', 'lastname'],
page: 3,
pagination: true
});
</script>

<script type="text/javascript">
var tableToList2 = new List('test-list2', {
valueNames: ['name', 'lastname'],
page: 3,
pagination: true
});
</script>

<script type="text/javascript">
var tableToList3 = new List('test-list3', {
valueNames: ['name', 'lastname'],
page: 3,
pagination: true
});
</script>

<script type="text/javascript">
var tableToList4 = new List('test-list4', {
valueNames: ['model', 'platenumber'],
page: 3,
pagination: true
});
</script>

<script>
var tableToList5 = new List('admin_list', {
valueNames: ['username', 'firstname', 'lastname'],
page: 9,
pagination: true
});
</script>

<script>
var tableToList6 = new List('ho_list', {
valueNames: ['firstname', 'lastname'],
page: 8,
pagination: true
});
</script>

<script>
var tableToList6 = new List('sg_list', {
valueNames: ['username','firstname', 'lastname'],
page: 10,
pagination: true
});
</script>

<script>
var tableToList7 = new List('street_name_list', {
valueNames: ['street_name', 'fullname'],
page: 3,
pagination: true
});
</script>

<script>
var tableToList8 = new List('street_name_list2', {
valueNames: ['name',],
page: 20,
pagination: true
});

</script>

<script>
var tableToList9 = new List('monthly_dues_list', {
valueNames: ['firstname', 'lastname', 'street_name'],
page: 10,
pagination: true
});
</script>


<script>
var tableToList10 = new List('announcement_list', {
valueNames: ['title', 'author'],
page: 10,
pagination: true
});
</script>

<script>
var tableToList10 = new List('activities_list', {
valueNames: ['title', 'author'],
page: 10,
pagination: true
});
</script>

<script>
var tableToList11 = new List('comments_list', {
//   valueNames: ['title', 'author'],
page: 10,
pagination: true
});
</script>

<script>
var tableToList12 = new List('repost_list', {
valueNames: ['title',],
page: 5,
pagination: true
});
</script>

<script>
var tableToList13 = new List('report_individual', {
valueNames: ['firstname', 'middlename', 'lastname'],
page: 5,
pagination: true
});
</script>

 <!--Script for modals-->
 <script type="text/javascript">


   jQuery(document).ready(function() {
   jQuery("time.timeago").timeago();

 });

// $(document).ready(function(){
// setInterval(function(){
// $("#show").load('includes/notif.php')
// }, 3000);
// });

         //   $('.itemName').select2({
         //   placeholder: 'Search a user',
         //   ajax: {
         //     url: 'includes/report_dropdown_data_individual_admin.php',
         //     dataType: 'json',
         //     delay: 250,
         //     processResults: function (data) {
         //       return {
         //         results: data
         //       };
         //     },
         //     cache: true
         //   }
         // });
</script>
<script>
$(document).ready(function(){

 $("#sel_depart").change(function(){
     var deptid = $(this).val();

     $.ajax({
         url: '../includes/ajaxData.php',
         type: 'post',
         data: {depart:deptid},
         dataType: 'json',
         success:function(response){

             var len = response.length;

             $("#sel_user").empty();
             for( var i = 0; i<len; i++){
                 var id = response[i]['id'];
                 var name = response[i]['name'];

                 $("#sel_user").append("<option value='"+id+"'>"+name+"</option>");

             }
         }
     });
 });

});
</script>
<script>
$(document).ready(function(){
   $('select').material_select();
 $('#street_name').on('change',function(){
     var ID = $(this).val();
     if(ID){
         $.ajax({
             type:'POST',
             url:'includes/ajaxData.php',
             data:'street_name_id='+ID,
             success:function(html){
                 $('#user_house_number').html(html);

             }
         });
     }
 });

</script>
<script>

   $(document).ready(function(){
     $('#modal').on('show.bs.modal', function (e) {
         var rowid = $(e.relatedTarget).data('id');
         $.ajax({
             type : 'post',
             url : 'modal_function.php',
             data :  'id='+ rowid, //Pass $id
             success : function(data){
             $('.data').html(data);//Show fetched data from database
             }
         });
      });
 });
</script>
     <script>

   $(document).ready(function(){
     $('.modal').modal({
         dismissible: true, // Modal can be dismissed by clicking outside of the modal
         opacity: .5, // Opacity of modal background
         inDuration: 300, // Transition in duration
         outDuration: 200, // Transition out duration
         startingTop: '1%', // Starting top style attribute
         endingTop: '10%', // Ending top style attribute
         ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
           alert("Ready");
           console.log(modal, trigger);
         },
         complete: function() { alert('Closed'); } // Callback for Modal close
       }
     );
   });

   </script>

<script>

     //for payment dues
      function change() {
           var fix = 850;
           var current = document.getElementById("currentDue").value;
           var forAdvance = document.getElementById("advance").value;
           var moneyGiven = document.getElementById("amountGiven").value;
           var months = +current + +forAdvance;
           var forTotal = fix * months;
           document.getElementById("totalDue").innerHTML = forTotal;

     }
 </script>



 <script>
      $('.datepicker').pickadate({
           selectMonths: true, // Creates a dropdown to control month
           selectYears: 50, // Creates a dropdown of 15 years to control year,
           today: 'Today',
           clear: 'Clear',
           close: 'Ok',
           closeOnSelect: false // Close upon selecting a date,
         });


</script>
<script>
     function homeowner(post_id,owner,address) {
       document.getElementsByName("p_id_req")[0].setAttribute("value", post_id);
       document.getElementById('address_req').innerText = address;
       document.getElementById('owner_req').innerText = owner;
   }
   </script>
   <script>

   function street_modal(street_name, id){
     document.getElementById('street').innerText = street_name;
     document.getElementById('id').innerText = id;

   }




 </script>

  <!--  <script>
     $(document).ready(function(){
          load_data();
        function load_data(query)
        {
         $.ajax({
          url:"./includes/search_admin_tbl.php",
          method:"POST",
          data:{query:query},
          success:function(data)
          {
           $('#result').html(data);
          }
         });
        }
        $('#search_text').keyup(function(){
         var search = $(this).val();
         if(search != '')
         {
          load_data(search);
         }
         else
         {
          load_data();
         }
        });
});

</script>
-->

<script>
function searchFilter(page_num) {
 page_num = page_num?page_num:0;
 var keywords = $('#keywords').val();
 var sortBy = $('#sortBy').val();
 $.ajax({
     type: 'POST',
     url: 'getData.php',
     data:'page='+page_num+'&keywords='+keywords+'&sortBy='+sortBy,
     beforeSend: function () {
         $('.loading-overlay').show();
     },
     success: function (html) {
         $('#posts_content').html(html);
         $('.loading-overlay').fadeOut("slow");
     }
 });
}
</script>
<script>

 //  $('#admin').DataTable({
 //      "Processing": true,
 //      "serverSide": true,

 //      lengthMenu: [[5, 10, 20], [5, 10, 20]],
 //         pageLength: 5,
 //         sPaginationType: "full_numbers",
 //      "ajax":{
 //         url :"./includes/search_admin_tbl.php",
 //         type: "post",  // type of method  ,GET/POST/DELETE
 //         error: function(){
 //           $("#admin").css("display","none");
 //         }
 //       }
 //     });


</script>
<script type="text/javascript">
     /*Show entries on click hide*/
     $(document).ready(function(){
         $(".dropdown-content.select-dropdown li").on( "click", function() {
             var that = this;
             setTimeout(function(){
             if($(that).parent().hasClass('active')){
                     $(that).parent().removeClass('active');
                     $(that).parent().hide();
             }
             },100);
         });
     });
 </script>
<script>
     // $(document).ready(function(){
     //     $('#example').DataTable({
     //        "Processing": true,
     //         "serverSide":true,
     //         "bFilter": true,
     //          "blengthChange": true,
     //          "binfo": true,
     //          lengthMenu: [[5, 10, 20, -1], [5, 10, 20, "All"]],
     //     pageLength: 5,

     //         "ajax":{
     //             url:"includes/search_admin_tbl.php",
     //             type:"post"
     //         }
     //     });
     // });
 </script>
 <!--script js for get edit data-->
 <script>
     $(document).on('click','#getEdit',function(e){
         e.preventDefault();
         var per_id=$(this).data('id');
         //alert(per_id);
         $('#content-data').html('');
         $.ajax({
             url:'editdata.php',
             type:'POST',
             data:'id='+per_id,
             dataType:'html'
         }).done(function(data){
             $('#content-data').html('');
             $('#content-data').html(data);
         }).fial(function(){
             $('#content-data').html('<p>Error</p>');
         });
     });
 </script>

 <script>
   $(function() {
         $.validator.setDefaults({
           highlight: function(element){
             $(element).addClass('invalid');
           },
           unhighlight: function(element){
             $(element).removeClass('valid');
           }
         });
         jQuery.validator.addMethod("lettersonly1", function(value, element) {
 return this.optional(element) || /^[a-z\s\-]+$/gi.test(value);
}, "Only alphabetical characters");

$.validator.addMethod("valueNotEquals", function(value, element, arg){
return arg !== value;
}, "Value must not equal arg.");

          $("#formValidate").validate({
             rules: {
                 firstname: {
                     required: true,
                     lettersonly1: true
                 },
                 middlename: {
                     required: true,
                     lettersonly1: true
                 },
                 lastname: {
                     required: true,
                     lettersonly1: true
                 },
                 birthdate: {
                     required: true,
                 },
                 mobile: {
                     required: true,
                     number: true
                 },
                 street: {
                     required: true,
                     valueNotEquals: 0

                 },
                 user_house_number: {
                     required: true,
                     select: true
                 },
                 gender:"required"
                 },
             //For custom messages
             messages: {
                 firstname:{
                     lettersonly1: "Invalid name.",
                     required: "Please enter your first name."
                 },
                 middlename:{
                     lettersonly1: "Invalid name.",
                     required: "Please enter your middle name."
                 },
                 mobile:{
                     required: "Please enter your mobile number."
                 },
                 birthdate:{
                     required: "Please enter your birth date."
                 },
                 lastname:{
                     lettersonly1: "Invalid name.",
                     required: "Please enter your last name."
                 },
                 street:{
                     required: "Please select street",
                 }
             },
             errorPlacement: function(error, element) {
              error.insertAfter(element.parent("div")).wrap('<div class="col s4 m4 l4" style="text-color: red; padding-top:45px;"></div>');
               // if (placement) {
               //   $(placement).append(error)
               // } else {
               //   error.insertAfter(element);
               // }
             },

              submitHandler: function(form) {

               var fields = {};
               $('#formValidate input, button, select').each(function(index, node) {
                var fieldName = $(node).attr('name');
                if (typeof fieldName !== 'undefined') {
                 // console.log(fieldName);
                 if (!(fieldName in fields)) {
                   fields[fieldName] = $(node).val();
                 }
                }
               });

               console.log('Posting fields', fields);
               fields['create_homeowner'] = true;

               $.ajax({
                 url: 'includes/add_ho_exec.php',
                 method: 'POST',
                 data: fields,
                 success: function(response) {
                   var result = JSON.parse(response);
                   if (result.added === true) {
                     swal("Congrats",result.message, "success").then(function() {
                         window.location.href = "users.php?source=ho_account&page=1";
                     });

                   } else {
                     swal("Sorry mate",result.message);
                   }

                   console.log(response, result);
                 },
                 error: function(errors) {
                   console.log(errors);
                 }
               });
             }


          });
   });

       </script>

        <script>
   $(function() {
         $.validator.setDefaults({
           highlight: function(element){
             $(element).addClass('invalid');
           },
           unhighlight: function(element){
             $(element).removeClass('valid');
           }
         });

         jQuery.validator.addMethod("lettersonly", function(value, element) {
             return this.optional(element) || /^[a-z]+$/i.test(value);
           });

          $("#formValidate_admin").validate({
             rules: {
                 firstname: {
                     required: true,
                     lettersonly: true
                 },
                 middlename: {
                     required: true,
                     number: false
                 },
                 lastname: {
                     required: true,
                     number: false
                 },
                 username: {
                     required: true,
                     number: false
                 },
                 password: {
                     required: true,
                     number: false
                 },
                 birthdate: {
                     required: true,
                     number: false
                 },

                 gender:"required",
                 },
             //For custom messages
             messages: {
                 firstname:{
                     lettersonly: "Invalid name."
                 },
                 middlename:{
                     lettersonly: "Invalid name."
                 },
                 lastname:{
                     lettersonly: "Invalid name."
                 },
             },

             errorElement : 'div',
             errorPlacement: function(error, element) {
               var placement = $(element).data('error');
               if (element.attr("type") == "radio") {
                   error.insertBefore(element);
               } else {
                   error.insertAfter(element);
               }
               // if (placement) {
               //   $(placement).append(error)
               // } else {
               //   error.insertAfter(element);
               // }
             },

              submitHandler: function(form) {

               var fields = {};
               $('#formValidate_admin input, button, select').each(function(index, node) {
                var fieldName = $(node).attr('name');
                if (typeof fieldName !== 'undefined') {
                 // console.log(fieldName);
                 if (!(fieldName in fields)) {
                   fields[fieldName] = $(node).val();
                 }
                }
               });

               console.log('Posting fields', fields);
               fields['create_admin'] = true;

               $.ajax({
                 url: 'includes/add_admin_exec.php',
                 method: 'POST',
                 data: fields,
                 success: function(response) {
                   var result = JSON.parse(response);
                   if (result.added === true) {
                     swal("Congrats",result.message, "success").then(function() {
                         window.location.href = "users.php?source=admin_account&page=1";
                     });

                   } else {
                     swal("Sorry mate",result.message);
                   }

                   console.log(response, result);
                 },
                 error: function(errors) {
                   console.log(errors);
                 }
               });
             }


          });
   });

       </script>
   <!-- script for editing sg accounts -->
        <script>
   $(function() {
         $.validator.setDefaults({
           highlight: function(element){
             $(element).addClass('invalid');
           },
           unhighlight: function(element){
             $(element).removeClass('valid');
           }
         });

         jQuery.validator.addMethod("lettersonly", function(value, element) {
             return this.optional(element) || /^[a-z]+$/i.test(value);
           });

          $("#").validate({
             rules: {
                 firstname: {
                     required: true,
                     lettersonly: true
                 },
                 middlename: {
                     required: true,
                     number: false
                 },
                 lastname: {
                     required: true,
                     number: false
                 },
                 username: {
                     required: true,
                     number: false
                 },
                 password: {
                     required: true,
                     number: false
                 },
                 birthdate: {
                     required: true,
                     number: false
                 },

                 gender:"required",
                 },
             //For custom messages
             messages: {
                 firstname:{
                     lettersonly: "Invalid name."
                 },
                 middlename:{
                     lettersonly: "Invalid name."
                 },
                 lastname:{
                     lettersonly: "Invalid name."
                 },
             },

             errorElement : 'div',
             errorPlacement: function(error, element) {
               var placement = $(element).data('error');
               if (element.attr("type") == "radio") {
                   error.insertBefore(element);
               } else {
                   error.insertAfter(element);
               }
               // if (placement) {
               //   $(placement).append(error)
               // } else {
               //   error.insertAfter(element);
               // }
             },

              submitHandler: function(form) {
var fields = {};
               $('# input, button, select').each(function(index, node) {
                var fieldName = $(node).attr('name');
                if (typeof fieldName !== 'undefined') {
                 // console.log(fieldName);
                 if (!(fieldName in fields)) {
                   fields[fieldName] = $(node).val();
                 }
                }
               });

               console.log('Posting fields', fields);
               fields['edit_sg'] = true;

// swal({
//   title: "Are you sure?",
//   text: "You will not be able to recover this imaginary file!",
//   type: "warning",
//   showCancelButton: true,
//   confirmButtonColor: "#DD6B55",
//   confirmButtonText: "Yes, delete it!",
//   cancelButtonText: "No, cancel plx!",
//   closeOnConfirm: false,
//   closeOnCancel: false
// },
//   function (isConfirm) {
//     if (isConfirm) {
     $.ajax({
       type: 'POST',
       url: 'includes/edit_guard_exec.php',
       data: fields,
       success: function(response) {
                   var result = JSON.parse(response);
                   if (result.added === true) {
                     swal("Congrats",result.message, "success").then(function() {
                         window.location.href = "users.php?source=admin_account&page=1";
                     });

                   } else {
                     swal("Sorry mate",result.message);
                   }

                   console.log(response, result);
                 },
                 error: function(errors) {
                   console.log(errors);
                 }
               });

// return false;


             }//end if handler


          // });
   });

       </script>


          <script>
   $(function() {
         $.validator.setDefaults({
           highlight: function(element){
             $(element).addClass('invalid');
           },
           unhighlight: function(element){
             $(element).removeClass('valid');
           }
         });

         jQuery.validator.addMethod("lettersonly", function(value, element) {
             return this.optional(element) || /^[a-z]+$/i.test(value);
           });

          $("#formValidate_sg").validate({
             rules: {
                 firstname: {
                     required: true,
                     lettersonly: true
                 },
                 middlename: {
                     required: true,
                     number: false
                 },
                 lastname: {
                     required: true,
                     number: false
                 },
                 username: {
                     required: true,
                     number: false
                 },
                 password: {
                     required: true,
                     number: false
                 },
                 birthdate: {
                     required: true,
                     number: false
                 },

                 gender:"required",
                 },
             //For custom messages
             messages: {
                 firstname:{
                     lettersonly: "Invalid name."
                 },
                 middlename:{
                     lettersonly: "Invalid name."
                 },
                 lastname:{
                     lettersonly: "Invalid name."
                 },
             },

             errorElement : 'div',
             errorPlacement: function(error, element) {
               var placement = $(element).data('error');
               if (element.attr("type") == "radio") {
                   error.insertBefore(element);
               } else {
                   error.insertAfter(element);
               }
               // if (placement) {
               //   $(placement).append(error)
               // } else {
               //   error.insertAfter(element);
               // }
             },

              submitHandler: function(form) {

           var fields = {};
             $('#formValidate_sg input, button, select').each(function(index, node) {
              var fieldName = $(node).attr('name');
              if (typeof fieldName !== 'undefined') {
               // console.log(fieldName);
               if (!(fieldName in fields)) {
                 fields[fieldName] = $(node).val();
               }
              }
             });

             console.log('Posting fields', fields);
             fields['edit_sg'] = true;

             swal({
               title: "",
               text: "You're about to update an account",
               type: "warning",
               showCancelButton: true,
               confirmButtonColor: "#ff9800",
               confirmButtonText: "Yes, continue",
               cancelButtonText: "No, cancel",
               closeOnConfirm: false,
               closeOnCancel: false
             },
               function (isConfirm) {
                 if (isConfirm) {
                   $.ajax({
                     type: 'POST',
                     url: 'includes/edit_guard_exec.php',
                     data: fields,
                     success: function(response) {
                                 var result = JSON.parse(response);
                                 if (result.added === true) {
                                   swal("Congrats",result.message, "success").then(function() {
                                       window.location.href = "users.php?source=guards_account";
                                   });

                                 } else {
                                   swal("Sorry mate",result.message);
                                 }

                                 console.log(response, result);
                               },
                               error: function(errors) {
                                 console.log(errors);
                               }
                   });
                 } else {
                   swal("Cancelled", "", "error");
                 }
               });

                 return false;


             }//end handler


          });
   });

       </script>



</body>
</html>

<?php

include "includes/modals.php";

?>
