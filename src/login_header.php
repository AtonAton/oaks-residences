<!--This html file is for the topbar header-->


    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                  <h1 class="logo-wrapper"><a href="/index.php" class="brand-logo darken-1"><img class="resize" src="/resources/images/oaks-logo-topbar.png" alt="Oaks Residences"></a></h1>
            </div>
        </nav>
    </div>
    <!-- end header nav-->
